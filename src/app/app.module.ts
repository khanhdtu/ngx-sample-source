import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRouterModule } from './router/app-router.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ModalModule } from 'ng2-modal';
import { MaterializeModule } from "angular2-materialize";

import { AppComponent } from './app.component';

import { AuthGuard } from './guards/index';
import { AuthService } from './service/auth/index';
import { ManagementService } from './service/management/index';
import { HttpService } from './helpers/httpService';
import { SharedService } from './service/shared.service';
import { SidebarObservable } from "./helpers/subject/sidebar.subject";

import { OrderBy } from "app/helpers/orderBy"
import { FilterPipe } from "app/helpers/filterBy"
import { TranslatePipe } from "app/helpers/translate"

import { LoginComponent, PasswordForgotComponent } from './component/auth/login/index';
import { RegisterComponent } from './component/auth/register/index';
import { HomeComponent } from './component/home/home/index';
import { BillComponent } from './component/home/management/bill/index';
import { AssetComponent } from './component/home/management/asset/index';
import { BackgroundComponent } from './component/home/management/background/index';
import { HistoryComponent } from './component/home/management/history/index';
import { FeedbackComponent } from './component/home/management/feedback/index';
import { QuestionComponent, QuestionAddComponent } from './component/home/management/question/index';
import { CriterionComponent, CriterionAddComponent } from './component/home/management/criterion/index';
import { MenuComponent, MenuAddComponent } from './component/home/management/menu/index';
import { QbankComponent, QbankAddComponent } from './component/home/management/qbank/index';
import { GiftComponent, GiftAddComponent, GiftImportComponent, GiftHistoryComponent } from './component/home/management/gift/index';
import { OptionComponent } from './component/home/management/option/index';
import { DiscountComponent, DiscountAddComponent } from 'app/component/home/management/discount/index';
import { InformationComponent } from './component/home/management/information/index';
import { CustomerComponent } from './component/home/management/customer/index';
import { PasswordChangeComponent } from './component/home/management/password/index';

import { HeaderComponent } from './component/header/index';
import { SidebarComponent } from './component/sidebar/index';
import { BreadcrumbComponent } from './component/breadcrumb/index';
import { FooterComponent } from './component/footer/index';

import { PreloaderComponent } from './component/preloader/index';
import { ChartComponent } from './component/home/management/chart/chart.component';
import { ChartDetailComponent } from './component/home/management/chart/chart-detail/chart-detail.component';
import { ReportComponent } from './component/home/management/report/report.component';
import { PromotionComponent } from './component/home/management/promotion/promotion.component';
import { StaffComponent } from './component/home/management/staff/staff.component';
import { StaffAddComponent } from './component/home/management/staff/staff.add.component';
import { PromotionAddComponent } from './component/home/management/promotion/promotion.add.component';
import { BookComponent } from './component/home/management/book/book.component';

import { CommonErrorHandler } from 'app/helpers/errorHandler';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PasswordForgotComponent,
    RegisterComponent,
    HomeComponent,
    AssetComponent,
    BillComponent,
    BackgroundComponent,
    HistoryComponent,
    FeedbackComponent,
    QuestionComponent,
    QuestionAddComponent,
    CriterionComponent,
    CriterionAddComponent,
    MenuComponent,
    MenuAddComponent,
    QbankComponent,
    QbankAddComponent,
    GiftComponent,
    GiftAddComponent,
    GiftImportComponent,
    GiftHistoryComponent,
    OptionComponent,
    DiscountComponent,
    DiscountAddComponent,
    InformationComponent,
    CustomerComponent,
    PasswordChangeComponent,
    //
    HeaderComponent,
    SidebarComponent,
    BreadcrumbComponent,
    FooterComponent,
    PreloaderComponent,
    ChartComponent,
    ChartDetailComponent,
    ReportComponent,
    PromotionComponent,
    StaffComponent,
    StaffAddComponent,
    PromotionAddComponent,
    BookComponent,
    // helpers
    OrderBy,
    FilterPipe,
    TranslatePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRouterModule,
    ModalModule,
    MaterializeModule,
    BrowserAnimationsModule,
    ToastModule.forRoot()
  ],
  providers: [
    { provide: ErrorHandler, useClass: CommonErrorHandler }, 
    AuthService, 
    ManagementService, 
    AuthGuard, 
    HttpService, 
    SharedService,
    SidebarObservable
],
  bootstrap: [AppComponent]
})

export class AppModule { }
