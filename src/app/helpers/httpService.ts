import { Injectable, ErrorHandler } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CommonErrorHandler } from 'app/helpers/errorHandler';
import { Router } from '@angular/router';

@Injectable()
export class HttpService {

  public username: string;
  public token: string;

  constructor(private http: Http, private router: Router, private errorHandler: ErrorHandler) { }

  createAuthorizationHeader(headers: Headers) {
    var currentUser = localStorage.getItem('currentUser');
    if (currentUser != null) {
      let token = JSON.parse(localStorage.getItem('currentUser')).accessToken;
      headers.append('Authorization', token);
    }
  }

  get(url) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    this.createAuthorizationHeader(headers);
    return this.http.get(url, { headers: headers });
  }

  post(url, criteria) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    this.createAuthorizationHeader(headers);
    return this.http.post(url, criteria, { headers: headers });
  }

  upload(url: string, formData: FormData) {
    // let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
    // headers.append('Accept', 'application/json');
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, formData, options);
  }

  getwithcallback(url, callback): Promise<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    this.createAuthorizationHeader(headers);
    return this.http.get(url, { headers: headers })
      .toPromise().then(res => {
        return callback(res);
      }, err => {
        let error = err.json();
        this.errorHandler.handleError(error);
        return Promise.reject(error.message);
      }).catch(this.handleError);
  }

  postwithcallback(url, criteria, callback) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    this.createAuthorizationHeader(headers);
    return this.http.post(url, criteria, { headers: headers })
      .toPromise()
      .then(res => {
        return callback(res);
      }, err => {
        let error = err.json();
        this.errorHandler.handleError(error);
        return Promise.reject(error.message);
      });
  }

  uploadwithcallback(url: string, formData: FormData, callback) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, formData, options)
      .toPromise()
      .then(res => {
        return callback(res);
      }, err => {
        console.log(err);
      });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}