import { ErrorHandler, Injectable, Injector } from "@angular/core";
import { MaterializeAction, toast } from 'angular2-materialize';
import { Router } from '@angular/router';
import { AuthService } from 'app/service/auth/index';

@Injectable()
export class CommonErrorHandler implements ErrorHandler {

  private router: Router;
  private authService: AuthService;
  constructor(injector: Injector) {
    setTimeout(() => {
      this.router = injector.get(Router);
      this.authService = injector.get(AuthService);
    });
  }

  handleError(error) {
    if (error.status == 403 || error.status == 400) {
      toast(error.message, 3000, 'toast-error');
      setTimeout(() => {
        this.authService.logout();
        this.router.navigate(['/login']);
      });
    }
  }
}