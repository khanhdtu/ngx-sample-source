import { Pipe, PipeTransform } from '@angular/core';
import { SharedService } from 'app/service/shared.service'; // our translate service

@Pipe({
    name: 'translate',
})

export class TranslatePipe implements PipeTransform {

    constructor(private _translate: SharedService) { }

    transform(value: string, args: any[]): any {
        if (!value) return;
        return this._translate.get(value);
    }
}