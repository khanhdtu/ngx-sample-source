import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterBy',
    pure: false
})

export class FilterPipe implements PipeTransform {
    transform(items: any, filter: any): any {
        if (filter && Array.isArray(items)) {
            let filterKeys = Object.keys(filter);
            return items.filter(item =>
                filterKeys.reduce((memo, keyName) =>
                    (memo && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === "", true));
            // items = items.filter(item => {
            //     console.log("A", item.filterKeys[0]);
            //     item.filterKeys[0] == filter[filterKeys[0]];
            // });
            //return items;
        } else {
            return items;
        }
    }
}