import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { Sidebar } from "../../model/sidebar.model";

@Injectable()
export class SidebarObservable{
    public sidebar : Sidebar;
    public sidebarObs = new Subject<Sidebar>();
    constructor(){
        this.sidebar = {
            isOpen : false
        }
        this.sidebarObs.next(this.sidebar);
    }

    setSidebar(sidebar : Sidebar){
        this.sidebar = sidebar;
        this.sidebarObs.next(this.sidebar);
    }

    getSidebar(){
        return this.sidebarObs.asObservable();
    }
}