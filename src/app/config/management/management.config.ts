import { AppConfig } from "../app.config";
export class ManagementConfig {
    public static GetProviderByUsernameUrl: string = AppConfig.ApiRoot + '/provider/profile';
    public static GetQRCodeUrl: string = AppConfig.ApiRoot + '/provider/GetQrCode';
    public static GetMessengerCodeUrl: string = AppConfig.ApiRoot + '/provider/messCode';
    public static GetHistoryUrl: string = AppConfig.ApiRoot + '/provider/history';
    public static GetFeedbackUrl: string = AppConfig.ApiRoot + '/provider/feedbacks';
    public static GetQuestionUrl: string = AppConfig.ApiRoot + '/provider/questions';
    public static GetOptionUrl: string = AppConfig.ApiRoot + '/provider/options';
    public static GetChartsUrl: string = AppConfig.ApiRoot + '/provider/chart';
    public static BuyCoinUrl: string = AppConfig.ApiRoot + '/provider/coin/buy';

    // CRITERION
    public static GetCriterionsUrl: string = AppConfig.ApiRoot + '/provider/criterions';
    public static GetCriterionByIdUrl: string = AppConfig.ApiRoot + '/provider/criterions/getById';
    public static AddCriterionUrl: string = AppConfig.ApiRoot + '/provider/criterions/add';
    public static DeleteCriterionUrl: string = AppConfig.ApiRoot + '/provider/criterions/delete';

    // MENU
    public static GetMenuUrl: string = AppConfig.ApiRoot + '/provider/menu';
    public static GetItemOnMenuUrl: string = AppConfig.ApiRoot + '/provider/menu/getItem';
    public static AddItemToMenuUrl: string = AppConfig.ApiRoot + '/provider/menu';
    public static DeleteItemOnMenuUrl: string = AppConfig.ApiRoot + '/provider/menu/delete';

    // IMAGE
    public static UploadImageUrl: string = AppConfig.ApiRoot + '/provider/image/upload';

    // REPORT
    public static GetReportsUrl: string = AppConfig.ApiRoot + '/provider/report'; 

    // PROMOTION
    public static GetPromotionUrl: string = AppConfig.ApiRoot + '/provider/promotions/get';
    public static AddPromotionUrl: string = AppConfig.ApiRoot + '/provider/promotions/add';
    public static GetAllPromotionsUrl: string = AppConfig.ApiRoot + '/provider/promotions/getAllPromotion';
    public static GetPromotionByIdUrl: string = AppConfig.ApiRoot + '/provider/promotions/getPromotionById';  
    
    //STAFF 
    public static GetMessengerCodeStaffUrl: string = AppConfig.ApiRoot + '/provider/messCodeStaff';
    public static GetStaffsUrl: string = AppConfig.ApiRoot + '/provider/staff';   
    public static DeleteStaffUrl: string = AppConfig.ApiRoot + '/provider/staff/delete';   

    // CUSTOMER
    public static GetCustomerUrl: string = AppConfig.ApiRoot + '/provider/customer';
    public static GetCustomerByIdUrl: string = AppConfig.ApiRoot + '/provider/customer/get';

    // QUESTION BANK - CUSTOMIZE
    public static GetQbankUrl: string = AppConfig.ApiRoot + '/provider/qbank';
    public static GetQbankByIdUrl: string = AppConfig.ApiRoot + '/provider/qbank/get';
    public static AddQbankUrl: string = AppConfig.ApiRoot + '/provider/qbank';  
    public static DeleteQbankUrl: string = AppConfig.ApiRoot + '/provider/qbank/delete';

    // GIFT
    public static GetGiftUrl: string = AppConfig.ApiRoot + '/provider/gift';
    public static GetGiftByIdUrl: string = AppConfig.ApiRoot + '/provider/gift/get';
    public static AddGiftUrl: string = AppConfig.ApiRoot + '/provider/gift/addOrUpdate';
    public static DeleteGiftByIdUrl: string = AppConfig.ApiRoot + '/provider/gift/delete';
    public static GetGiftByCodeUrl: string = AppConfig.ApiRoot + '/provider/gift/getByCode';
    public static UseGiftCodeUrl: string = AppConfig.ApiRoot + '/provider/gift/use';
    public static GetGiftHistoryUrl: string = AppConfig.ApiRoot + '/provider/gift/history';
    
    //BOOK
    public static GetBooksUrl: string = AppConfig.ApiRoot + '/provider/books/getBooks';    
    public static updateBookUrl: string = AppConfig.ApiRoot + '/provider/books/updateBook';    

    //BADCOMMENT
    public static ResponsesBadcommentUrl: string = AppConfig.ApiRoot + '/provider/responsesBadcomment';    

    // PASSWORD
    public static ChangePasswordUrl: string = AppConfig.ApiRoot + '/provider/password';
    
    // DISCOUNT
    public static GetDiscountUrl: string = AppConfig.ApiRoot + '/provider/discount';
    public static GetDiscountByIdUrl: string = AppConfig.ApiRoot + '/provider/discount/get';
    public static AddOrUpdateDiscountUrl: string = AppConfig.ApiRoot + '/provider/discount/addOrUpdate';
    
}