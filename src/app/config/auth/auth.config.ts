import { AppConfig } from "../app.config";
export class AuthConfig {
    public static LoginUrl : string = `${AppConfig.ApiRoot}/auth/login`;
    public static RegisterUrl : string = `${AppConfig.ApiRoot}/auth/register`;
    public static ResetPasswordUrl : string = `${AppConfig.ApiRoot}/auth/password`;
}