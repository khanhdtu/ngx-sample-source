import { Headers } from '@angular/http'
export class AppConfig {
    public static ApiRoot: string = "http://localhost:3000";
    // public static ApiRoot: string = "http://roulette2017.herokuapp.com";
    public static StaticRoot: string = "http://gonjoy.io/static/roulette";
    public static Headers = new Headers({ 'Content-Type': 'application/json' });
}