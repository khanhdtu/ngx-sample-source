import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../guards/index';
import { HeaderComponent } from '../component/header/index';
import { SidebarComponent } from '../component/sidebar/index';
import { BreadcrumbComponent } from '../component/breadcrumb/index';
import { PreloaderComponent } from '../component/preloader/index';
import { FooterComponent } from '../component/footer/index';

import { LoginComponent, PasswordForgotComponent } from '../component/auth/login/index';
import { RegisterComponent } from '../component/auth/register/index';
import { HomeComponent } from '../component/home/home/index';
import { BillComponent } from '../component/home/management/bill/index';
import { AssetComponent } from '../component/home/management/asset/index';
import { BackgroundComponent } from '../component/home/management/background/index';
import { HistoryComponent } from '../component/home/management/history/index';
import { FeedbackComponent } from '../component/home/management/feedback/index';
import { QuestionComponent, QuestionAddComponent } from '../component/home/management/question/index';
import { MenuComponent, MenuAddComponent } from '../component/home/management/menu/index';
import { QbankComponent, QbankAddComponent } from '../component/home/management/qbank/index';
import { CriterionComponent, CriterionAddComponent } from '../component/home/management/criterion/index';
import { GiftComponent, GiftAddComponent, GiftImportComponent, GiftHistoryComponent } from '../component/home/management/gift/index';
import { OptionComponent } from '../component/home/management/option/index';
import { DiscountComponent, DiscountAddComponent } from '../component/home/management/discount/index';
import { ChartComponent } from '../component/home/management/chart/index';
import { ChartDetailComponent } from '../component/home/management/chart/chart-detail/index';
import { ReportComponent } from '../component/home/management/report/index';
import { PromotionComponent, PromotionAddComponent } from '../component/home/management/promotion/index';
import { StaffComponent, StaffAddComponent } from '../component/home/management/staff/index';
import { CustomerComponent } from '../component/home/management/customer/index';
import { InformationComponent } from '../component/home/management/information/index';
import { BookComponent } from '../component/home/management/book/index';
import { PasswordChangeComponent } from '../component/home/management/password/index';

const routes: Routes = [
    { path: '', component: HeaderComponent, outlet: 'header' },
    { path: '', component: FooterComponent, outlet: 'footer' },
    // { path: '', component: BreadcrumbComponent, outlet: 'breadcrumb' },
    // { path: '', component: PreloaderComponent, outlet: 'preloader' },
    { path: '', redirectTo: '/login', pathMatch: "full" },
    { path: 'login', component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "chart", component: ChartComponent },
    { path: "chartDetail/:id", component: ChartDetailComponent },
    {
        path: 'home', component: HomeComponent, canActivate: [AuthGuard]
    },
    {
        path: 'report', component: ReportComponent, canActivate: [AuthGuard]
    },
    {
        path: 'bill', component: BillComponent, canActivate: [AuthGuard]
    },
    {
        path: 'asset', component: AssetComponent, canActivate: [AuthGuard]
    },
    {
        path: 'background', component: BackgroundComponent, canActivate: [AuthGuard]
    },
    {
        path: 'history', component: HistoryComponent, canActivate: [AuthGuard]
    },
    {
        path: 'feedback', component: FeedbackComponent, canActivate: [AuthGuard]
    },
    {
        path: 'option', component: OptionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'discount', component: DiscountComponent, canActivate: [AuthGuard]
    }
    ,
    {
        path: 'discount/:id', component: DiscountAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'criterion', component: CriterionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'criterion/add', component: CriterionAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'criterion/:id', component: CriterionAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'menu', component: MenuComponent, canActivate: [AuthGuard]
    },
    {
        path: 'menu/add', component: MenuAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'menu/:id', component: MenuAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'question', component: QuestionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'question/add', component: QuestionAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'promotion', component: PromotionAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'promotion/add/:id', component: PromotionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'staff', component: StaffComponent, canActivate: [AuthGuard]
    },
    {
        path: 'staff/add', component: StaffAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'customer', component: CustomerComponent, canActivate: [AuthGuard]
    },
    {
        path: 'qbank', component: QbankComponent, canActivate: [AuthGuard]
    },
    {
        path: 'qbank/add', component: QbankAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'qbank/:id', component: QbankAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'gift', component: GiftComponent, canActivate: [AuthGuard]
    },
    {
        path: 'gift/add', component: GiftAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'gift/import', component: GiftImportComponent, canActivate: [AuthGuard]
    },
    {
        path: 'gift/history', component: GiftHistoryComponent, canActivate: [AuthGuard]
    },
    {
        path: 'gift/:id', component: GiftAddComponent, canActivate: [AuthGuard]
    },
    {
        path: 'coin/buy', component: GiftAddComponent, canActivate: [AuthGuard]
    }
    ,
    {
        path: 'information', component: InformationComponent, canActivate: [AuthGuard]
    },
    {
        path: 'book', component: BookComponent, canActivate: [AuthGuard]
    },
    {
        path: 'password', component: PasswordChangeComponent, canActivate: [AuthGuard]
    }
    ,
    {
        path: 'password/forgot', component: PasswordForgotComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})

export class AppRouterModule { }