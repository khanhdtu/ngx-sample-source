export class Report {
    day:any    
    informations: any;
    reportDaily: any;
    reportWeek: any;
    badComments: any;

    constructor(public jsonStr: string) {
        let jsonObj = JSON.parse(jsonStr);
        this.day = jsonObj.day;
        this.informations = jsonObj.informations;
        this.reportDaily=jsonObj.reportDaily;
        this.reportWeek = jsonObj.reportWeek;
        this.badComments = jsonObj.badComments;
    }
}