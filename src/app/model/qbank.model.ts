import { Answer } from 'app/model/answer.model';

export class Qbank{
    content: String;
    answers: Answer[];
    isDeleted: Boolean;
    createdAt: Date;
    createdBy: String;
    updatedAt: Date;
    updatedBy: String;

    constructor(){
        
    }
}