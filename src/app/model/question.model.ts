import { Answer } from 'app/model/answer.model';

export class Question {
    content: String;
    isDeleted: Boolean;
    createdAt: Date;
    createdBy: String;
    updatedAt: Date;
    updatedBy: String;
}