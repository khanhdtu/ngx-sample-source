export class Answer {
    content: String;
    isImage: Boolean;
    isCorrect: Boolean;

    constructor(){
        this.isImage = false;
        this.isCorrect = false;
    }
}