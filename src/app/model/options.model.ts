export class Options {
    id: number;
    content: string;
    value: number;
    minxValue: number;
    maxValue: number;
    isNoteRequired: boolean;
    answerBackgroundUrl: string;

    constructor(public jsonStr?: string) {
        let jsonObj = JSON.parse(jsonStr);
        this.id = jsonObj.id;
        this.content = jsonObj.content;
        this.value = jsonObj.value;
        this.minxValue = jsonObj.minxValue;
        this.maxValue = jsonObj.maxValue;
        this.isNoteRequired = jsonObj.isNoteRequired;
        this.answerBackgroundUrl = jsonObj.answerBackgroundUrl;
    }
}