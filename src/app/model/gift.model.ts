export class Gift {
    _id: string;
    name: string;
    thumbnail: string;
    coin: number;
    amount: number;
    expireDate: Date;
    status: string;
    type: string;
    code: string;
    isDeleted: boolean;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
}