export class Feedback {
    providerUsername: string;
    customerName: string;
    isDeleted: boolean;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    discount: {
        ces: {
            nCorrect: Number,
            percent: Number
        },
        rank: {
            nCorrect: Number,
            percent: Number
        },
        total: {
            percent: Number
        },
        refNo: Number,
        billValue: Number,
        discountValue: Number
    };
    report: {
        csat: Number,
        ces: [{
            criteriaId: Number,
            value: Number
        }],
        nps: Number,
        rank: [{
            criteriaId: Number,
            value: Number,
            comment: String
        }]
    };
}