export class Menu {
    _id: string;
    name: string;
    description: string;
    thumbnail: string;
    price: number;
    rate: string;
    isDeleted: boolean;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
}