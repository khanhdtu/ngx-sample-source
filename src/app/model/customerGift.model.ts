import { Gift } from 'app/model/gift.model';

export class CustomerGift {
    _id: string;
    code: string;
    status: string;
    isExpired: boolean;
    providerGift: Gift;
    constructor(){
        this.isExpired = true;
    }
}