export class Criterion {
    _id: string;
    name: string
    weightage: Number;
    rankQuestion: string;
    iconName: string;
    isUpload: boolean;
    active: boolean;
    isDeleted: boolean;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    // food, service, price, others
    type: string;

    constructor(){
        this.active = false;
    }
}