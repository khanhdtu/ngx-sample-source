export class Chart {    
    id: string;
    name: string;
    type: string;
    data: string;
    blockquote: string;
    small_quote:string;
    level:number;

    constructor(public jsonStr: string) {
        let jsonObj = JSON.parse(jsonStr);
        this.id = jsonObj.id;
        this.name=jsonObj.name;
        this.type = jsonObj.type;
        this.data = jsonObj.data;
    	this.blockquote=jsonObj.blockquote;
        this.small_quote = jsonObj.small_quote;
        this.level=jsonObj.level;
    }
}