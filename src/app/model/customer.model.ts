export class Customer {
    name: string;
    gender: string;
    avatarUrl: string;
    visits: number;
}