export class ProviderModel {    
    public username: string;
    public name: string;
    public email: string;
    public address: string;
    public tel: string;
    public coin: Number;
    public background: string;
    public lastestCustomerLimit: string;
    public restriction: string;

    constructor(public jsonStr?: string) {
        if (jsonStr != undefined && jsonStr != null){
            let jsonObj = JSON.parse(jsonStr);
            this.username = jsonObj.username;
            this.name = jsonObj.name;
            this.email = jsonObj.email;
            this.address = jsonObj.address;
            this.tel = jsonObj.tel;
            this.background = jsonObj.background;
            this.lastestCustomerLimit = jsonObj.latestCusomterLimit;
            this.restriction = jsonObj.restriction;
        }
    }
}