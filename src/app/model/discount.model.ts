export class Discount {
    nCorrect: number;
    percent: string;
    coin: number;
    expression: number;
    isDeleted: number;
    createdAt: Date;
    updatedAt: Date;
}