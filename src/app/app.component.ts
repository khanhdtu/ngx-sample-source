import { Component } from '@angular/core';
import { RouterModule, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  template:
  `<router-outlet name="preloader"></router-outlet>
  <div *ngIf="!isAuth">
    <router-outlet name="header"></router-outlet>
  </div>
  <div id="main">
    <div class="wrapper">
      <div *ngIf="!isAuth">
        <app-sidebar></app-sidebar>
      </div>
      <section id="content">
        <div class="container">
          <router-outlet></router-outlet> 
        </div>
        <div *ngIf="!isAuth">
        <router-outlet name="footer"></router-outlet>
      </div>
      </section>
    </div>
  </div>`
})

export class AppComponent {
  isAuth: boolean = false;
  constructor(
    private router: Router
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        var url = event.urlAfterRedirects.substr(1);
        this.isAuth = url == 'login' || url == 'register' || url == 'password/forgot';
      }
    });
  }
}
