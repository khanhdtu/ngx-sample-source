export class MessageConstants {
    public static Inprogress = "I am on the process, please wait...";
    public static Loading = "Wait a second, I am loading...";
    public static ChangeBgSuccess = "Changed background successfully!";
    public static SthGoWrong = "Something goes wrong, please try again...";

}