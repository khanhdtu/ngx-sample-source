export class NavigationConstants {
    public static Login = 'login';
    public static Home = 'home';
    public static Bill = 'bill';
    public static History = 'history';
    public static Feedback = 'feedback';
    public static Chart = 'chart';
    public static ChartDetail = 'chartDetail';
    public static Promotion = 'promotion';
    public static Staff = 'staff';
    public static AddStaff = 'staff/add';
    public static Menu = 'menu';
    public static Qbank = 'qbank';
    public static Criterion = 'criterions';
    public static Gift = 'gift';
}