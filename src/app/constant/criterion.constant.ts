export class CriterionTypeConstants {
    public static Food = 'Food';
    public static Service = 'Service';
    public static Price = 'Price';
    public static Others = 'Others';
}