import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NavigationConstants } from 'app/constant/navigation.constant';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})

export class BreadcrumbComponent {

  public navbarName: string;
  public navbarLevel3Name: string;
  constructor(
    private router: Router
  ) { 
     this.router.events.subscribe(event => {
        if(event instanceof NavigationEnd) {
          if(this.navbarLevel3Name) delete this.navbarLevel3Name;
          var url = event.url;
          if (url.indexOf(NavigationConstants.Login) > 0) this.navbarName = "Login";
          if (url.indexOf(NavigationConstants.Home) > 0) this.navbarName = "Information";
          if (url.indexOf(NavigationConstants.Bill) > 0) this.navbarName = "Bill";
          if (url.indexOf(NavigationConstants.History) > 0) this.navbarName = "History";
          if (url.indexOf(NavigationConstants.Feedback) > 0) this.navbarName = "Feedback";
          if (url.indexOf(NavigationConstants.Chart) > 0) this.navbarName = "Chart";
          if (url.indexOf(NavigationConstants.ChartDetail) > 0) this.navbarLevel3Name = "Chart Detail";
          if (url.indexOf(NavigationConstants.Promotion) > 0) this.navbarName = "Promotion";
          if (url.indexOf(NavigationConstants.Staff) > 0) this.navbarName = "Staff";
          if (url.indexOf(NavigationConstants.AddStaff) > 0) this.navbarLevel3Name = "Add Staff";
          if (url.indexOf(NavigationConstants.Menu) > 0) this.navbarName = "Menu";
          if (url.indexOf(NavigationConstants.Qbank) > 0) this.navbarName = "Customize";
          if (url.indexOf(NavigationConstants.Criterion) > 0) this.navbarName = "Criterions";
          if (url.indexOf(NavigationConstants.Gift) > 0) this.navbarName = "Gifts";
          
        }
    });
  }
}
