import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {
  DomSanitizer,
  SafeHtml,
  SafeUrl,
  SafeStyle
} from '@angular/platform-browser';

import { ManagementService } from 'app/service/management/index';
import { SharedService } from 'app/service/shared.service';
import { AuthService } from 'app/service/auth/index';
import { ProviderModel } from 'app/model/provider.model';
import { AppConfig } from "app/config/app.config";
import { RestrictionConstants } from 'app/constant/restriction.constant';

declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {
  provider: ProviderModel = new ProviderModel();
  activeLink: string;
  sidenavActions = new EventEmitter<any>();
  sidenavParams = [];
  hasFullRestriction: boolean = false;
  bg: string = 'assets/images/sp_default_thumbnail.jpg';
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;
  image: SafeStyle;

  constructor(
    private managementService: ManagementService,
    private authService: AuthService,
    private sharedService: SharedService,
    private router: Router,
    private sanitization: DomSanitizer
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.activeLink = event.url.substr(1);
      }
    });
  }

  ngOnInit() {
    var str = localStorage.getItem('currentUser');
    if (str != null) {
      this.provider = new ProviderModel(str);
      if (this.provider.background){
        this.bg = this.apiUrl + '/public/img/upload/bg/' + this.provider.background;
      }
      this.image =  this.sanitization.bypassSecurityTrustStyle(`url(${this.bg})`)       
    }

    this.hasFullRestriction = this.sharedService.restriction == RestrictionConstants.Full;
  }

  showSidenav(e): void {
    if (e.target.className.indexOf('clickable') > -1)
      $("#sidenav-overlay").trigger("click");
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
}
