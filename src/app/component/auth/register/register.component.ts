import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, Validators } from '@angular/forms';

import { MaterializeAction, toast  } from 'angular2-materialize';

import { ProviderModel } from 'app/model/provider.model';

import { AuthService } from 'app/service/auth/auth.service';

import { RestrictionConstants } from 'app/constant/restriction.constant';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  provider: ProviderModel = new ProviderModel();
  loading: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.provider.restriction = RestrictionConstants.Full;
  }

  register(): void {
    this.authService.register(this.provider)
    .then((res) => {
      let success = res["result"];
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
        if(success){
          toast(res["message"], 3000, 'toast-success');
          this.router.navigate(["/login"]);
        }else{
          toast(res["message"], 3000, 'toast-error');
        }
      }, 1000);
    });
  }
}
