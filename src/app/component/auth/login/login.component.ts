import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AuthService } from 'app/service/auth/auth.service';
import { SharedService } from 'app/service/shared.service';

import { RestrictionConstants } from 'app/constant/restriction.constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  username: string;
  password: string;
  errorMessage: string;
  loading: boolean = false;

  constructor(
    public toastr: ToastsManager,
    private authService: AuthService,
    private router: Router,
    private vcr: ViewContainerRef,
    private sharedService: SharedService
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var provider = localStorage.getItem('currentUser');
    if (provider != null) {
      this.router.navigate(["/home"]);
    }
  }

  login(): void {
    this.loading = true;
    this.authService.login(this.username, this.password)
      .then((success: boolean) => {
        this.sharedService.getLocalizations().then(() => {
          this.loading = false;
          setTimeout(() => {
            if (success) {
              var hasFullRestriction = this.sharedService.restriction == RestrictionConstants.Full;
              if (hasFullRestriction)
                this.router.navigate(["/home"]);
              else
                this.router.navigate(["/information"]);
            } else {
              this.errorMessage = 'Username or password is incorrect';
              this.toastr.error(this.errorMessage, 'Oops! ');
            }
          }, 1000);
        });
      });
  }
}
