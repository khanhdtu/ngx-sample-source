import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

import { MaterializeAction, toast  } from 'angular2-materialize';

import { AuthService } from 'app/service/auth/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './password.forgot.component.html',
  styleUrls: ['./login.component.css']
})

export class PasswordForgotComponent {
  username: string;
  message: string;
  loading: boolean = false;

  constructor(
    private authService: AuthService,
  ) {
  }

  reset(): void{
    this.loading = true;
    this.authService.resetPassword(this.username).then(res => {
      this.loading = false;
      let success = res["success"];
      let type = success ? 'toast-success' : 'toast-error';
      toast(res["message"], 3000, type);
    });
  }

}
