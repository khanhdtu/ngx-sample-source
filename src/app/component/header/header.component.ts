import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { ManagementService } from 'app/service/management/index';
import { AuthService } from 'app/service/auth/index';
import { ProviderModel } from 'app/model/provider.model';

import { MaterializeDirective } from 'angular2-materialize';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  provider: ProviderModel = new ProviderModel();
  activeLink: string;

  constructor(
    private managementService: ManagementService,
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    var str = localStorage.getItem('currentUser');
    if (str != null) {
      this.provider = new ProviderModel(str);
    }
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
}
