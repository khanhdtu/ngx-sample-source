import { Component, Input} from '@angular/core';

// @Component({
//   selector: 'app-preloader',
//   templateUrl: './preloader.component.html',
//   styleUrls: ['./preloader.component.css'],
//    styles:[
//     `.c3-axis-y .domain, .c3-axis-y .tick>line
// {
//     display: none;
// }
//     `
//   ]
// })
@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.css']
})

export class PreloaderComponent {
  @Input() loading: boolean = false;
  @Input() inform: string = "Wait a minute, I am loading...";
}
