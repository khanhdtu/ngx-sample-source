import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
import { ManagementService } from 'app/service/management/index';
import { Chart } from 'app/model/chart.model';
import { Report } from 'app/model/report.model';
import { AppConfig } from "app/config/app.config";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ProviderModel } from 'app/model/provider.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  informations: any;
  reportDaily: any;
  reportWeek: any;
  badComments: any;
  today: string;
  staffs:any;
  isCus:Boolean;
  message:String;
  comment:any;
  provider: ProviderModel = new ProviderModel();
  
  constructor(
    private managementService: ManagementService,
    private router: Router,
    private vcr: ViewContainerRef,    
    public toastr: ToastsManager
  ) {
    this.toastr.setRootViewContainerRef(vcr);          
  }

  ngOnInit() {
    var self = this;
    this.managementService.getReports().then(report => {
      this.managementService.getStaffs().then(staffs => {
        this.staffs=staffs;
        self.getValueByDefault(report);        
      });     
    });

    // Create WebSocket connection.
    const socket = new WebSocket(AppConfig.ApiRoot.replace("http", "ws") + '/websocket/report/' + JSON.parse(localStorage.getItem('currentUser')).username);
    // Listen for messages
    socket.addEventListener('message', function (event) {
      self.getValueByDefault(JSON.parse(JSON.parse(event.data)));
    });
    //this.getValueByDefault(a);
    this.managementService.getProfile().then(res => {
      this.provider = res;
    });
  }

  getValueByDefault(report) {
    var self=this;    
    this.today = report.today;
    this.informations = report.informations;
    this.reportDaily = report.reportDaily;
    this.reportWeek = report.reportWeek;
    this.badComments = report.badComments;
    if(!this.staffs) return;
    if (this.badComments)
      this.badComments.forEach(badComment=>{
        badComment.comments.forEach(comment =>{
            comment.staff=getStaffFromStaffFbId(comment.staffFbId);
        })
      })

    function getStaffFromStaffFbId(fbId){
      let staffs=self.staffs.filter(staff=>
        {
          return staff.fbId==fbId
        })
      if(staffs.length>0) return staffs[0];
      else return null;
    }
  }

  onClickReponse(isCus:Boolean,comment:any){
    if(isCus==true) this.isCus=true;
    if(isCus==false) this.isCus=false;  
    this.comment=comment;
  }

  sendMessage(){
    if (!this.message || this.message=="") return;    
    let dest;
    if(this.isCus){
      dest=this.comment.userId;
    }else{
      dest=this.comment.staff._id;
    }
    let response={
      isCus:this.isCus,
      message:this.message,
      idDest:dest,
      comment:this.comment
    }
    this.managementService.responsesBadcomment(response)
    .then((success: boolean) => {
      if (success){
        this.message="";      
        if (this.isCus==true){
          this.comment.isSentCus=true;          
        }else{
          this.comment.isSentStaff=true;                    
        }
        // this.loadHomePage()
      }
      else {
        this.toastr.error('Your input is not correct, please check your business...', 'Oops! ');
      }
    });
    this.message="";    
  }
  pressEnterKey(){
    this.sendMessage();
    $("#btnSend").trigger("click");
  }
  loadHomePage(){
    this.router.navigate(["/home"]);    
  }
}
