import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ButtonConstants } from 'app/constant/button.constant';
import { FormStatus } from 'app/constant/form.status';

import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';
import { Gift } from 'app/model/gift.model';
import { CustomerGift } from 'app/model/customerGift.model';
import { Criteria } from 'app/model/criteria.model';

@Component({
  selector: 'app-history-gift',
  templateUrl: './gift.history.component.html',
  styleUrls: ['./gift.component.css']
})

export class GiftHistoryComponent implements OnInit {
  gifts: CustomerGift[];
  criteria: Criteria = new Criteria();
  page: number = 1;
  totalPage: number = 1;

  constructor(
    private managementService: ManagementService,
  ) {
  }

  ngOnInit() {
    this.getGiftHistory(this.criteria);
  }
  
  getGiftHistory(criteria): void {
    this.managementService.getGiftHistory(criteria).then(p => {
      this.gifts = p['data'];
      this.totalPage = p['totalPage'];
    });
  }

  nextPage(): void {
    if (this.page < this.totalPage) this.page++;
    this.criteria = new Criteria(this.page);
    this.getGiftHistory(this.criteria);
  }

  previousPage(): void {
    if (this.page > 1) this.page--;
    this.criteria = new Criteria(this.page);
    this.getGiftHistory(this.criteria);
  }
}