import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ButtonConstants } from 'app/constant/button.constant';
import { FormStatus } from 'app/constant/form.status';

import { MaterializeAction, toast } from 'angular2-materialize';

import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';
import { Gift } from 'app/model/gift.model';

import { GiftTypeConstants } from 'app/constant/giftType.constant';

@Component({
  selector: 'app-add-gift',
  templateUrl: './gift.add.component.html',
  styleUrls: ['./gift.component.css']
})

export class GiftAddComponent implements OnInit {
  item: Gift = new Gift();
  submitButtonText: string = ButtonConstants.Create;
  loading: boolean = false;
  isValid: boolean = true;
  isFree: boolean = false;
  file: File = null;
  maxCoin: Number = 0;
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;
  createForm: FormGroup;

  constructor(
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.createForm = fb.group({
      'name': ['name_default', [Validators.required, Validators.minLength(6)]],
      'coin': ['', Validators.required],
      'amount': ['', Validators.required],
      'expireDate': ['', Validators.required],
    });
  }

  ngOnInit() {
    this.managementService.getProfile()
      .then(res => {
        this.maxCoin = res.coin;
      });

    let itemId = this.activedRoute.snapshot.params.id;
    if (itemId != undefined && itemId != 0) {
      this.submitButtonText = ButtonConstants.Update;
      this.managementService.getGiftById(itemId)
        .then(res => {
          this.item = res;
        });
    }
  }

  changeType(): void{
    this.isFree = false;
    if (this.item.type == GiftTypeConstants.Free){
        this.isFree = true;
        this.item.coin = 0;
        this.item.amount = 0;
    }
  }

  addOrUpdate(): void {
    this.loading = true;
    // Upload image
    let formData: FormData = null;
    if (this.file != null) {
      formData = new FormData();
      formData.append('uploadFile', this.file, this.file.name);
    }
    //
    this.managementService.addGift(this.item, formData)
      .then((res) => {
        this.loading = false;
        var success = res["success"];
        var giftType = res["type"];
        var code = res["code"];
        if (success)
        {
          if (giftType == GiftTypeConstants.Free){
            toast("Here your code for this free gift: " + code, 3000, 'toast-success');
          }
          this.router.navigate(["/gift"]);
        }
          
      });
  }

  fileChange(event): void {
    let _URL = window.URL;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];

      // Validate upload file
      let img = new Image();

      img.onload = () => {
        let width = img.naturalWidth;
        let height = img.naturalHeight;
        if (width != height) {
          this.isValid = false;
          toast('Oops! Your file is not correct, please check your business...', 3000, 'toast-error');
        }
      };
      img.src = _URL.createObjectURL(this.file);
    }
  }
}