import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';

import { ModalModule } from "ng2-modal";
import { AppConfig } from "app/config/app.config";

import { ManagementService } from 'app/service/management/index';
import { Gift } from 'app/model/gift.model';
import { Criteria } from 'app/model/criteria.model';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.css']
})

export class GiftComponent implements OnInit {
  gifts: Gift[];
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;
  page: number = 1;
  totalPage: number;
  criteria: Criteria = new Criteria();

  constructor(
    private managementService: ManagementService
  ) { 
  }

  ngOnInit() {
    this.getGifts(this.criteria);
  }

  nextPage(): void {
    if (this.page < this.totalPage) this.page++;
    this.criteria = new Criteria(this.page);
    this.getGifts(this.criteria);
  }

  previousPage(): void {
    if (this.page > 1) this.page--;
    this.criteria = new Criteria(this.page);
    this.getGifts(this.criteria);
  }

  getGifts(criteria): void {
    this.managementService.getGifts(this.criteria)
    .then(res => {
      this.gifts = res['data'];
      this.totalPage = res['totalPage'];
    });
  }

  delete(item: Gift): void {
    if(confirm("Are you sure to delete this gift?")) {
      this.managementService.deleteGift(item._id)
      .then(p =>{
        let idx = this.gifts.indexOf(item);
        this.gifts.splice(idx, 1);
      });
    }
  }
}