import { Component, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterializeAction, toast  } from 'angular2-materialize';

import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';
import { Gift } from 'app/model/gift.model';
import { CustomerGift } from 'app/model/customerGift.model';

@Component({
  selector: 'app-import-gift',
  templateUrl: './gift.import.component.html',
  styleUrls: ['./gift.component.css']
})

export class GiftImportComponent {
  apiUrl: string = AppConfig.ApiRoot;
  gift: CustomerGift = new CustomerGift();
  staticUrl: string = AppConfig.StaticRoot;
  giftCode: string = '';
  today: Date = (new Date());
  modalActions = new EventEmitter<string | MaterializeAction>();

  constructor(
    private managementService: ManagementService
  ) {
  }

  getGiftCode(): void {
    this.managementService.getGiftByCode(this.giftCode)
      .then(res => {
        this.gift = res;
        let expireDate = new Date(this.gift.providerGift.expireDate);
        if (expireDate.setHours(0, 0, 0, 0) < this.today.setHours(0, 0, 0, 0)) {
          this.gift.isExpired = true;
        } else {
          this.gift.isExpired = false;
        }
      })
  }

  useGiftCode(): void {
    this.managementService.useGiftCode(this.giftCode)
      .then(res => {
        if (res){
          this.giftCode = '';
          toast('Use the code successfully.', 3000, 'toast-success');
          this.modalActions.emit({ action: "modal", params: ['close'] });
        }else{
          toast('Something goes wrong, please check the code...', 3000, 'toast-error');
        }
      })
  }
}