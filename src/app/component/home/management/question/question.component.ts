import { Component, OnInit, ViewChild} from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import {ModalModule} from "ng2-modal";

import { ManagementService } from '../../../../service/management/index';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})

export class QuestionComponent implements OnInit {
  questions : Object[];

  constructor(
    private managementService: ManagementService,
  ) {
  }

  ngOnInit() {
    this.managementService.getQuestions().then(p => {
       this.questions = p;
      });
  }
}