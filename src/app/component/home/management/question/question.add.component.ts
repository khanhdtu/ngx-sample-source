import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ButtonConstants } from 'app/constant/button.constant';

import { ManagementService } from 'app/service/management/index';
import { Question } from 'app/model/question.model';

@Component({
  selector: 'app-add-question',
  templateUrl: './question.add.component.html',
  styleUrls: ['./question.component.css']
})

export class QuestionAddComponent implements OnInit {
  item: Question = new Question();
  submitButtonText: string = ButtonConstants.Create;
  noAns: number = 0;
  loading: boolean = false;

  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {

  }

  addOrUpdate(): void {

  }
}