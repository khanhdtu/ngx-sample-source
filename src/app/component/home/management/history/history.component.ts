import { Component, OnInit, ViewChild} from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { ManagementService } from '../../../../service/management/index';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})

export class HistoryComponent implements OnInit {
  histories : Object[];

  constructor(
    private managementService: ManagementService
  ) {
  }

  ngOnInit() {
    this.managementService.getHistories().then(p => {
       this.histories = p;
       console.log(p);
      });
  }
}