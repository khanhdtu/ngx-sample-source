import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
import { ManagementService } from '../../../../service/management/index';
import { Chart } from '../../../../model/chart.model';
import { Report } from '../../../../model/report.model';
import { AppConfig } from "../../../../config/app.config";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  informations: any;
  reportDaily: any;
  reportWeek: any;
  badComments: any;
  today: string;
  staffs:any;
  constructor(
    private managementService: ManagementService,
    private router: Router
  ) {
  }

  ngOnInit() {
    $(document).ready(function () {
      $('.collapsible').collapsible();
    });
    var self = this;
    this.managementService.getReports().then(report => {
      this.managementService.getStaffs().then(staffs => {
        this.staffs=staffs;
        console.log(staffs);
        console.log(report);
        self.getValueByDefault(report);        
      });     
    });
    // Create WebSocket connection.
    const socket = new WebSocket(AppConfig.ApiRoot.replace("http", "ws") + '/websocket/report/' + JSON.parse(localStorage.getItem('currentUser')).username);
    // Listen for messages
    socket.addEventListener('message', function (event) {
      self.getValueByDefault(JSON.parse(JSON.parse(event.data)));
    });
    //this.getValueByDefault(a);
  }
  getValueByDefault(report) {
    this.today = report.today;
    this.informations = report.informations;
    this.reportDaily = report.reportDaily;
    this.reportWeek = report.reportWeek;
    this.badComments = report.badComments;
  }
}
