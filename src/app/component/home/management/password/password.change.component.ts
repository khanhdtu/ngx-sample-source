import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { MaterializeAction, toast } from 'angular2-materialize';

import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';

@Component({
  selector: 'app-change-password',
  templateUrl: './password.change.component.html',
  styleUrls: ['./password.component.css']
})

export class PasswordChangeComponent {
  oldPassword: String;
  newPassword: String;
  reNewPassword: String;
  constructor(
    private managementService: ManagementService
  ) {
  }

  change(): void {
    this.managementService.changePassword(this.oldPassword, this.newPassword)
      .then(res => {
        this.oldPassword = '';
        this.newPassword = '';
        this.reNewPassword = '';
        let success = res["success"];
        if (success)
          toast(res["message"], 3000, 'toast-success');
        else
          toast(res["message"], 3000, 'toast-error');
      });
  }
}