import { Component, OnInit,NgZone, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import {Location} from '@angular/common';

import { ModalModule } from "ng2-modal";

import { ManagementService } from 'app/service/management/index';
@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
  staffs:any;
  constructor( 
    private managementService: ManagementService,
        private router: Router,
    private _location: Location,
    private zone: NgZone    

  ){}

  ngOnInit() {
    this.managementService.getStaffs().then(p => {
      this.staffs=p;
    });
  }
  delete(staff){
    this.managementService.deleteStaff(staff._id.toString()).then(cur=>{
      this.reloadPage()});
  }
  reloadPage() { // click handler or similar
        this.zone.runOutsideAngular(() => {
            location.reload();
        });
    }
}
