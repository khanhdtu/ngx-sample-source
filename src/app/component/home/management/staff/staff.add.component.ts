import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import {Location} from '@angular/common';

import { ModalModule } from "ng2-modal";

import { ManagementService } from 'app/service/management/index';
@Component({
  selector: 'app-add-staff',
  templateUrl: './staff.add.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffAddComponent implements OnInit {
  messengerCodeUri: string;  
  constructor(
    private managementService: ManagementService,
    private router: Router,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private _location: Location    
  ) { 
    this.toastr.setRootViewContainerRef(vcr);    
  }

  ngOnInit() {
    this.getMessengerCode();
  }
  getMessengerCode(): void {
    this.managementService.getMessengerCodeStaff()
      .then((res: any) => {
        this.messengerCodeUri = res.uri;
        console.log(this.messengerCodeUri);
      });
  }
  back():void{
    this._location.back();
  }
}
