import { Component, ViewContainerRef, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { ManagementService } from 'app/service/management/index';
import { MessageConstants } from 'app/constant/message.constant';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.css']
})

export class BackgroundComponent {
  loading: boolean = false;
  file: File = null;
  fileName: String;
  isDisabled: boolean = true;
  hasPreview: boolean = true;
  loadingInform: string = MessageConstants.Inprogress;

  constructor(
    private managementService: ManagementService,
    private router: Router,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private element: ElementRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  addOrUpdate(): void {
    this.loading = true;
    // Upload image
    let formData: FormData = null;
    if (this.file != null) {
      formData = new FormData();
      formData.append('uploadFile', this.file, this.file.name);
    }
    //
    this.managementService.uploadBackground(formData)
      .then((success) => {
        this.loading = false;
        if (success){
          this.hasPreview = false;
          this.fileName = '';
          this.toastr.success(MessageConstants.ChangeBgSuccess);
        }
        else {
          this.toastr.error(MessageConstants.SthGoWrong, 'Oops! ');
        }
        this.isDisabled = true;
      });
  }


  fileChange(event): void {
    this.isDisabled = false;
    this.hasPreview = true;
    let _URL = window.URL;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];
      var image = this.element.nativeElement.querySelector('.preview-image');
      // Validate upload file
      let img = new Image();

      img.onload = () => {
        image.src = img.src;
      };
      img.src = _URL.createObjectURL(this.file);
    }
  }
}