import { Component, OnInit } from '@angular/core';
import { ManagementService } from 'app/service/management/management.service';
import { FormsModule } from '@angular/forms';
import { ProviderModel } from 'app/model/provider.model';

import { SharedService } from 'app/service/shared.service';

import { RestrictionConstants } from 'app/constant/restriction.constant';

import { MaterializeAction, toast  } from 'angular2-materialize';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})

export class InformationComponent implements OnInit {
  provider: ProviderModel = new ProviderModel();
  hasFullRestriction: boolean = false;
  file: File = null;
  fileName: String;
  isDisabled: boolean = true;

  constructor(
    private managementService: ManagementService,
    private sharedService: SharedService) { 
  }

  ngOnInit() {
    var provider = localStorage.getItem('currentUser');
    if (provider != null) {
       this.managementService.getProfile().then(res => {
         this.provider = res;
       });
    }
    this.hasFullRestriction = this.sharedService.restriction == RestrictionConstants.Full;
  }

  changeBackground(): void {
    // Upload image
    let formData: FormData = new FormData();
    if (this.file != null) {
      formData.append('uploadFile', this.file, this.file.name);
    }
    //
    this.managementService.uploadBackground(formData)
      .then((success) => {
        if (success){
          toast("Successfully!", 3000, "toast-success");
        }
        this.fileName = '';
        this.isDisabled = true;
      });
  }

  fileChange(event): void {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];
      this.isDisabled = false;
    }
  }
}
