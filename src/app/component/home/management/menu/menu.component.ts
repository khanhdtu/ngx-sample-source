import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';

import { ModalModule } from "ng2-modal";
import { AppConfig } from "app/config/app.config";

import { ManagementService } from 'app/service/management/index';
import { Menu } from 'app/model/menu.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {
  menu: Menu[];
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;
  @ViewChild("DeleteModal") modal;

  constructor(
    private managementService: ManagementService
  ) {
  }

  ngOnInit() {
    this.managementService.getMenu()
      .then(res => {
        this.menu = res;
      });
  }

  delete(item: Menu): void {
    if (confirm("Are you sure to delete this item?")) {
      this.managementService.deleteItemOnMenu(item._id)
        .then(p => {
          let idx = this.menu.indexOf(item);
          this.menu.splice(idx, 1);
        });
    }
  }
}