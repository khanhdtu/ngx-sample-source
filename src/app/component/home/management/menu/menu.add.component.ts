import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ButtonConstants } from 'app/constant/button.constant';
import { FormStatus } from 'app/constant/form.status';

import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';
import { Menu } from 'app/model/menu.model';

@Component({
  selector: 'app-add-menu',
  templateUrl: './menu.add.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuAddComponent implements OnInit{
  item: Menu = new Menu();
  submitButtonText: string = ButtonConstants.Create;
  loading: boolean = false;
  isValid: boolean = true;
  file: File = null;
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;
  createForm: FormGroup;

  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.toastr.setRootViewContainerRef(vcr);
    this.createForm = fb.group({
      'name': ['name_default', Validators.required],
      'price': ['', Validators.required],
      'description': ['', Validators.required]
    });
  }

  ngOnInit() {
    let itemId = this.activedRoute.snapshot.params.id;
    if (itemId != undefined && itemId != 0) {
      this.submitButtonText = ButtonConstants.Update;
      this.managementService.getItemOnMenuById(itemId)
        .then(res => {
          this.item = res;
        });
    }
  }

  addOrUpdate(): void {
    if (!this.isValid) return;
    this.loading = true;
    // Upload image
    let formData: FormData = null;
    if (this.file != null) {
      formData = new FormData();
      formData.append('uploadFile', this.file, this.file.name);
    }
    //
    this.managementService.addItemToMenu(this.item, formData)
      .then((success: boolean) => {
        this.loading = false;
        if (success)
          this.router.navigate(["/menu"]);
      });
  }

  fileChange(event): void {
    let _URL = window.URL;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];

      // Validate upload file
      let img = new Image();

      img.onload = () => {
        let width = img.naturalWidth;
        let height = img.naturalHeight;
        if (width != height) {
          this.isValid = false;
          this.toastr.error('Your file is not correct, please check your business...', 'Oops! ');
        } else {
          this.isValid = true;
          this.toastr.info('Right...', 'Oops! ');
        }
      };
      img.src = _URL.createObjectURL(this.file);
    }
  }
}