import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { ModalModule } from "ng2-modal";
import { AppConfig } from "app/config/app.config";

import { ManagementService } from 'app/service/management/index';
import { Criterion } from 'app/model/criterion.model';

@Component({
  selector: 'app-criterion',
  templateUrl: './criterion.component.html',
  styleUrls: ['./criterion.component.css']
})

export class CriterionComponent implements OnInit {
  criterions: Criterion[];
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;

  constructor(
    private managementService: ManagementService,
  ) { 
  }

  ngOnInit() {
    this.managementService.getCriterions().then(p => {
      this.criterions = p;
    });
  }

  deleteCriterion(criterion: Criterion): void {
    this.managementService.deleteCriterion(criterion._id)
      .then(p =>{
        let idx = this.criterions.indexOf(criterion);
        this.criterions.splice(idx, 1);
      });
  }
}