import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ButtonConstants } from 'app/constant/button.constant';
import { FormStatus } from 'app/constant/form.status';
import { CriterionTypeConstants } from 'app/constant/criterion.constant';
import { ManagementService } from 'app/service/management/index';
import { Criterion } from 'app/model/criterion.model';

@Component({
  selector: 'app-add-criterion',
  templateUrl: './criterion.add.component.html',
  styleUrls: ['./criterion.component.css']
})

export class CriterionAddComponent implements OnInit {
  criterions: Criterion[];
  criterion: Criterion = new Criterion();
  iconName: string;
  icons: string[] = ["atmosphere.png", "food.png", "price.png", "reservation.png", "service.png", "timing.png", "others.png"];
  loading: boolean = false;
  submitButtonText: string = ButtonConstants.Create;
  type: string;
  file: File = null;
  isValid: boolean = true;
  isFixedCriterion: boolean = false;
  iconSelection: number;
  createForm: FormGroup;
  formStatus: FormStatus = FormStatus.Create;
  bfActive: boolean;

  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.toastr.setRootViewContainerRef(vcr);
    this.createForm = fb.group({
      'name': ['name_default', [Validators.required, Validators.minLength(3)]],
      'rankQuestion': ['', [Validators.required, Validators.minLength(10)]]
    });
  }

  ngOnInit() {
    let criterionId = this.activedRoute.snapshot.params.id;
    if (criterionId != undefined && criterionId != 0) {
      this.formStatus = FormStatus.Update;
      this.submitButtonText = ButtonConstants.Update;
      this.managementService.getCriterionById(criterionId)
        .then(res => {
          this.criterion = res;
          this.bfActive = this.criterion.active;
          //
          if (!this.isFixed(this.criterion.name)) {
            this.type = 'Others';
          } else {
            this.type = this.criterion.name;
            this.isFixedCriterion = true;
          }
        });
    }
    this.loading = true;
    this.managementService.getCriterions()
      .then(res => {
        this.criterions = res;
        this.loading = false;
      });
  }

  addOrUpdate(): void {
    if (!this.criterion.name || !this.criterion.rankQuestion || !this.isValid) {
      this.toastr.error('Please check your input before creating...', 'Oops!');
      return;
    }

    if (this.isExisting(this.criterion.name) && this.formStatus == FormStatus.Create) {
      this.toastr.error('Name is existed, please check your input...', 'Oops!');
      return;
    }

    if (!this.isFixed(this.type) && this.isFixed(this.criterion.name)) {
      this.toastr.error('Name is not valid, please check your input...', 'Oops!');
      return;
    }

    this.loading = true;
    // Upload image
    let formData: FormData = null;
    if (this.file != null) {
      formData = new FormData();
      formData.append('uploadFile', this.file, this.file.name);
    }
    //
    this.managementService.addCriterion(this.criterion, formData)
      .then((success: boolean) => {
        this.loading = false;
        if (success)
          this.router.navigate(["/criterion"]);
        else {
          this.toastr.error('Your input is not correct, please check your business...', 'Oops!');
        }
      });
  }

  fileChange(event): void {
    let _URL = window.URL;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];

      // Validate upload file
      let img = new Image();

      img.onload = () => {
        let width = img.naturalWidth;
        let height = img.naturalHeight;
        if (width != height) {
          this.isValid = false;
          this.toastr.error('Width and height of the image must be equal...', 'Oops! ');
        }
      };
      img.src = _URL.createObjectURL(this.file);
    }
  }

  bindingData(type: string): void {
    this.criterion = new Criterion();
    this.isFixedCriterion = this.isFixed(type);
    if (this.isFixed(type)) {
      this.criterion = this.criterions.find(c => c.name === type);
    } else {
      this.criterion.weightage = this.getOthersWeightage(this.criterion.active);
    }
  }

  isFixed(name: string): boolean {
    return name == CriterionTypeConstants.Food || name == CriterionTypeConstants.Service || name == CriterionTypeConstants.Price;
  }

  isExisting(name: string): boolean {
    let arr = this.criterions.map(c => c.name);
    return arr.indexOf(name) > -1;

  }

  getOthersWeightage(isActive: boolean): Number {
    let cnt = this.criterions.filter(c => c.active == true).length;
    if (isActive) return 0.2 / (cnt - 2);
    return 0;
  }

  getWeightage(isActive: boolean): void {
    let cnt = this.criterions.filter(c => c.active == true).length - 3;
    //
    if (isActive && this.formStatus == FormStatus.Create) {
      this.criterion.weightage = Math.round(0.2 / (cnt + 1) * 1000) / 1000;
      return;
    }
    //
    if (this.formStatus == FormStatus.Update) {
      if (isActive) {
        if (this.bfActive) this.criterion.weightage = Math.round(0.2 / (cnt) * 1000) / 1000;
        else this.criterion.weightage = Math.round(0.2 / (cnt + 1) * 1000) / 1000;
      }
      else {
        if (this.bfActive) this.criterion.weightage = Math.round(0.2 / (cnt - 1) * 1000) / 1000;
        else this.criterion.weightage = Math.round(0.2 / (cnt) * 1000) / 1000;
      }
      return;
    }
    this.criterion.weightage = 0;
  }
}