import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, Validators } from '@angular/forms';

import { MaterializeAction, toast  } from 'angular2-materialize';

import { ManagementService } from 'app/service/management/index';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})

export class BillComponent implements OnInit {
  billNumber: number = 1;
  qrCodeUri: string;
  messengerCodeUri: string;
  loading: boolean = false;
  loadingInform: string = "I am on the process, please wait...";
  modalActions = new EventEmitter<string | MaterializeAction>();

  constructor(
    private managementService: ManagementService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  getQRCode(): void {
    this.managementService.getQRCode(this.billNumber)
      .then((res: string) => {
        this.qrCodeUri = res;
      });
  }

  getMessengerCode(): void {
    this.loading = true;
    this.managementService.getMessengerCode(this.billNumber)
      .then(res => {
        this.modalActions.emit({ action: "modal", params: ['open'] });
        this.loading = false;
        this.messengerCodeUri = res['uri'];
      }, err => {
        this.loading = false;
      });
  }

  IsInteger(value) {
    value = value.trim();
    if ((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
      return true;
    } else {
      return false;
    }
  }
}