import { Component, OnInit, ViewContainerRef,NgZone} from '@angular/core';
import { FormsModule, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
declare var $:any;
import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ButtonConstants } from 'app/constant/button.constant';
import { FormStatus } from 'app/constant/form.status';
import { ManagementService } from 'app/service/management/index';
import { Criterion } from 'app/model/criterion.model';
@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.css']
})
export class PromotionComponent implements OnInit {
  customers:any;
  loading: boolean = false;
  file: File = null;
  isValid: boolean = true;
  customerSelected:any =[];
  isAll:boolean=false;
  title:String="";
  content:any="";
  link:String="";
  isNow:boolean=true;
  time:number;
  timeData:any;
  timePromotion:any;
  dateBegin:any;
  dateEnd:any;
  submitButtonText:any=ButtonConstants.Create;
  promotion:any;
  isUpload:boolean=true;
  isUpdate:boolean=false;
  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private zone: NgZone        
  ) { 
     this.toastr.setRootViewContainerRef(vcr);  
  }

  ngOnInit() {
    $(document).ready(function() {
      $('select').material_select();
    });
    var self=this;
    var $input=$('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15, // Creates a dropdown of 15 years to control year,
      today: 'Today',
      clear: 'Clear',
      close: 'Ok',
      closeOnSelect: false,
      format: 'dd /mm/ yyyy'      
    });
    let promotionId = this.activedRoute.snapshot.params.id;
    if (promotionId != undefined && promotionId != 0) {
      this.isUpdate=true;
      this.submitButtonText = ButtonConstants.Update;
      this.managementService.getPromotionById(promotionId)
        .then(res => {
          this.promotion = res;
          this.title=this.promotion.title;
          this.link=this.promotion.link;
          this.content=this.promotion.description;
          $("#dateBegin").val(this.promotion.dateBegin);        
          $("#dateEnd").val(this.promotion.dateEnd);     
        });
    }

      // this.managementService.getCustomers().then(p => {
      //   this.customers = p//this.getCustomers(p);
      // });
  }
  // getCustomers(customers):Object[]{
  //     var listUser=[];
  //          var customerIds=[];
  //            customers.forEach(customer =>{
  //                   if(customerIds.indexOf(customer.customerId)>=0){
  //                       //ADD If need number visits
  //                       listUser[customerIds.indexOf(customer.customerId)].numerVisit++;
  //                   }else{
  //                       customerIds.push(customer.customerId);
  //                       listUser.push({
  //                           customerId:customer.customerId,
  //                           customerName:customer.customerName,
  //                           numerVisit:1,
  //                           active:false
  //                       })                        
  //                   }
  //               })
  //          return listUser;
  // }

  addOrUpdate(): void {
    if (!this.isValid) return;
    this.loading = true;
    // Upload image
    let formData: FormData = null;
    if (this.file != null) {
      formData = new FormData();
      formData.append('uploadFile', this.file, this.file.name);
    }
    this.dateBegin = new Date($("#dateBegin").val());   
    this.dateEnd = new Date($("#dateEnd").val());    
    var promotion={
      _id:0,
      description:this.content,
      title:this.title,
      link:this.link,
      dateBegin:$("#dateBegin").val(),
      dateEnd:$("#dateEnd").val()
      
      // customerSelected: (this.isAll) ? this.getAllIDCustomer() : this.customerSelected,
      // dateBegin:this.dateBegin.getDate()+"/"+(this.dateBegin.getMonth()+1)+"/"+this.dateBegin.getFullYear(),
      // dateEnd:this.dateEnd.getDate()+"/"+(this.dateEnd.getMonth()+1)+"/"+this.dateEnd.getFullYear()
    }
    if (this.promotion && this.promotion._id) promotion._id=this.promotion._id;
    //this.dateBegin = new Date($("#dateBegin").val());
    // console.log(promotion);
    // if (this.isAll) console.log(this.getAllIDCustomer());
    // else console.log(this.customerSelected);

    this.managementService.addPromotion(promotion, formData)
      .then((success: boolean) => {
        if (success){
           alert("Your promotion is adding !!!");
           this.loadPromotionPage()
          //  this.reloadPage(); 
        }
        else {
          this.toastr.error('Your input is not correct, please check your business...', 'Oops! ');
        }
      });
  }

  fileChange(event): void {
    let _URL = window.URL;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList[0];

      // Validate upload file
      let img = new Image();

      img.onload = () => {
        let width = img.naturalWidth;
        let height = img.naturalHeight;
      };
      img.src = _URL.createObjectURL(this.file);
    }
  }

  // onCheck(customer):void{
  //   var pos=this.customerSelected.indexOf(customer.customerId)
  //   if(customer.active){
  //       this.customerSelected.push(customer.customerId);
  //   }else{
  //       if (pos>-1){
  //           this.customerSelected.splice(pos,1);
  //       } 
  //   }
  // }
  // selectAll(isAll):void{
  //   if(isAll){
  //       this.isAll=true;
  //       this.customerSelected=[];
  //   }else{
  //       this.isAll=false;
  //   }
  //}
  isUploadImage(isUpload):void{
    if(isUpload){
        this.isUpload=true;
    }else{
        this.isUpload=false;
    }
  }
  getAllIDCustomer():any{
    var list=this.customers.map(customer => {return customer.customerId});
    return list
  }
  reloadPage() { // click handler or similar
    this.zone.runOutsideAngular(() => {
        location.reload();
    });
  }
  loadPromotionPage(){
    this.router.navigate(["/promotion"]);    
  }
}