import { Component, OnInit, ViewContainerRef,NgZone} from '@angular/core';
import { FormsModule, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
declare var $:any;
import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';
import { Criterion } from 'app/model/criterion.model';
@Component({
  selector: 'app-add-staff',
  templateUrl: './promotion.add.component.html',
  styleUrls: ['./promotion.component.css']
})
export class PromotionAddComponent implements OnInit {
  promotions:any;
  apiUrl: string = AppConfig.ApiRoot;  
  staticUrl: string = AppConfig.StaticRoot;
  page: number = 1;
  totalPage:number;
  
  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private zone: NgZone        
  ) { 
     this.toastr.setRootViewContainerRef(vcr);  
  }

  ngOnInit() {
    this.getPromotions();    
      // this.managementService.getPromotions(this.page).then(p => {
      //   console.log(p)
      //   this.promotions = p['data'];;//this.getCustomers(p);
      // });
  }
  reloadPage() { // click handler or similar
    this.zone.runOutsideAngular(() => {
          console.log("reload");
        location.reload();
    });
  }
  nextPage(): void {
    if (this.page < this.totalPage) this.page++;
    this.getPromotions();    
  }

  previousPage(): void {
    if (this.page > 1) this.page--;
    this.getPromotions();
  }

  getPromotions(): void {
      this.managementService.getPromotions(this.page).then(p => {
        console.log(p)
        this.promotions = p['data'];;//this.getCustomers(p);
        this.totalPage = p['totalPage'];        
      });
    }
}