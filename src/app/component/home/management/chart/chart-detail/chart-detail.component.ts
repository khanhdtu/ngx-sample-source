import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import * as D3 from 'd3';
import * as C3 from 'c3';
declare var $:any;
import { ManagementService } from 'app/service/management/index';
import { Chart } from 'app/model/chart.model';
import { ActivatedRoute, ParamMap,Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppConfig } from "app/config/app.config";

@Component({
  selector: 'chart-detail',
  templateUrl: './chart-detail.component.html',
  styleUrls: ['./chart-detail.component.css'],
})
export class ChartDetailComponent implements OnInit {
   chart : any;
   chartDetail:any;
   noteDetail:any;
   isExtra: boolean;
   idChart:string;
   listCharts:Chart[];
   isCreatedChart:boolean;
   constructor(
       private managementService: ManagementService,
       private route: ActivatedRoute,
       private location: Location,
       private _router:Router
   ){}

  ngOnInit() {
     var self=this;
     this.route.params.subscribe(params => {
              this.idChart = params['id'];
              self.managementService.getCharts().then(charts => {
                 var chart=self.getChart(self.idChart,charts);  
                 self.chart=chart[0];
                 self.chartDetail=self.getChart(self.idChart+"_detail",charts);   
                 self.isExtra =(self.chart.type=="extra") ? true: false;
                 self.noteDetail=self.getNoteDetail(self.chartDetail[0].id);
                 self.afterViewInit();
              });
        });
    }
  getNoteDetail(id){
      var good;
      var bad;
      var normal;
      var show=true;
      switch(id) {
        case "csat_detail":
            good='Satisfied';
            normal='Neutral';
            bad='Unsatisfied';
            break;
        case "nps_detail":
             good='Promoters';
             normal='Neutrals';
             bad='Detractors';
             break;
        case "ces_detail":
             show=false;
            break;
        default:
             good='Good';
             normal='Normal';
             bad='Bad';
       }
       return {  
          good:good,
          normal:normal,
          bad:bad,
          show:show
        }    
  }  
  getChart(id,charts): Chart{
    let chart= charts.filter((chart) => {return chart.id==id});
    return chart
    }

  afterViewInit(){
        var self=this;

      // Create WebSocket connection.
        const socket = new WebSocket(AppConfig.ApiRoot.replace("http", "ws")+'/websocket/chart/'+JSON.parse(localStorage.getItem('currentUser')).username);
  
     // Listen for messages
        socket.addEventListener('message', function (event) {
           self.listCharts=JSON.parse(event.data);
           self.chart=self.listCharts.filter((chart)=>{return chart.id==self.idChart})[0];
           self.chartDetail=self.listCharts.filter((chart)=>{return chart.id==self.idChart+"_detail"})[0];
           if(!self.isCreatedChart) createCharts(self.chart,self.chartDetail);
           else loadCharts(self.chart,self.chartDetail);
        });
        var DonutChart;
        var BarChart;
        var scoreWeek;
        //create charts when open
        function createCharts(chart,chartDetail){
            if(chart.type=="extra"){
              DonutChart=self.createDonutChart("#idDonutChartDetail",chart.data.circle,self.getColorFromLevel(chart.level));
              BarChart=self.createBarChartRotated("#idBarChartDetail",chart.data.bar);
              scoreWeek=self.createScoreWeek("#ExtraScoreWeekDetail",chartDetail.data);
            }else 
            {
              if (chartDetail.id!="ces_detail"){
                 DonutChart=self.createDonutChart("#WeekScoreDonutDetail",chartDetail.data.circle,self.getColorFromLevel(chartDetail.level));
              }else{
                $(".clValueCES").empty();
                $(".clValueCES").append("<span>"+chartDetail.data.circle+"</span>");
              }
              BarChart=self.createBarChartFromNumberColumns("#WeekScoreBarDetail",chartDetail.data.bar,chartDetail.id,chartDetail.data.legend||"");
              scoreWeek=self.createScoreWeek("#WeekScoreChartDetail",chart.data);
            }  
            self.isCreatedChart=true;      
        }
        //reload charts.
        function loadCharts(chart,chartDetail){
             if(chart.type=="extra"){
                   DonutChart=self.createDonutChart("#idDonutChartDetail",chart.data.circle,self.getColorFromLevel(chart.level));
                   BarChart=self.createBarChartRotated("#idBarChartDetail",chart.data.bar);
                   scoreWeek.load({
                    columns:chartDetail.data
                    })
              }else {
                   if (chartDetail.id!="ces_detail"){
                       DonutChart=self.createDonutChart("#WeekScoreDonutDetail",chartDetail.data.circle,self.getColorFromLevel(chartDetail.level));
                   }else{
                        $(".clValueCES").empty();
                        $(".clValueCES").append("<span>"+chartDetail.data.circle+"</span>");
                   }
                   BarChart=self.createBarChartFromNumberColumns("#WeekScoreBarDetail",chartDetail.data.bar,chartDetail.id,chartDetail.data.legend||"");
                    scoreWeek.load({
                    columns:chart.data
                    })
              }
            //$(".c3-axis-y .domain").css("display","none");    
            //$(".c3-axis-y .tick>line").css("display","none");
        }
    }
    getColorFromLevel(level){
        switch(level) {
                  case 1:
                    return '#4caf50'
                  case 0:
                   return '#fdc141'
                  case -1:
                    return '#f7665b'
                }
    }
    createScoreWeek(idDiv,data:any) {
        return C3.generate({
                bindto: idDiv,
                padding: {
                      top: 10,
                      right: 10,
                      bottom: 40,
                      left: 25
                },
                size: {
                      height: 300
                },
                data: {
                    x:'x',
                    columns: data,
                    type: 'area'
                      ,
                    labels: true
                },
                legend: {
                    show: false
                },
                color: {
                      pattern: ['#8edcf7']
                },
                axis: {
                      y: {
                        min: -80,
                        max: 80,
                        tick:{
                            outer: false
                        }
                    },
                     x: {
                     type: 'categories'
                    }
                }
            });    
    }
    createBarChartRotated(idDiv :string,data:any) {
          var max = data.reduce(function(a, b) {
              return Math.max(a, b);
          });
          return C3.generate({
          bindto: idDiv,
          data: {
            columns: [data],
            type: 'bar',
            labels: true,
            colors: {
              data1: function(d) {
                switch(d.x) {
                  case 0:
                     return '#f7665b';
                  case 1:
                     return '#fdc141';
                  case 2:
                      return '#4caf50';
                  default:
                      return '#1f77b4';
                }
              }
            }
          },
          legend: {
              show: false
           },
          padding: {
             left: -20
          },
          bar: {
             width: 5
          },
          axis: {
            rotated: true,
            y: {
                  min: 0,
                  max: max,
                  tick:{
                        outer: true
                      },
                  show:false
              }
          },tooltip: {
            show:false
          }
      });
    }
    createDonutChart(idDiv :string, percentage: number,color:any) {
        $(idDiv).empty();
        return C3.generate({
        bindto: idDiv,
        padding: {
                      top: 10,
                      right:30,
                      left: 0,
                      bottom:0
                },
        data: {
           columns: [
               ['data1', percentage],
               ['data2', 100-percentage]
            ],
           type : 'donut',
             order: null,
           colors: {
            data1: color,
            data2: '#f3f0f0',
        },
        },
        legend: {
              show: false
        },
        donut: {
          title: percentage+"%",
          width: 5,
          expand: false,
          label: {
              show: false
          }
        },tooltip: {
            show:false
          }
    });        
    }

    createBarChartFromNumberColumns(idDiv :string,data:any,id:string,legend:any) {
         var type='';
         var dataX:any;
         switch(id) {
                  case "csat_detail":
                    type='category';
                    dataX=["x","Very Unsatisfied", "Unsatisfied", "Neutral", "Satisfied", "Very Satisfied"];
                    break;
                  case "nps_detail":
                    type='category';
                    dataX=["x","0","1", "2", "3", "4", "5","6","7","8","9","10"];
                    break;
                  case "ces_detail":
                    type='category';
                    legend.unshift("x");
                    dataX=legend;
                    break;
           }
          return C3.generate({
          bindto: idDiv,
          data: {
              x : 'x',
              columns: [dataX,data],
            type: 'bar',
            labels: true,
            colors: {
              data1: function(d) {
                return getColor(d.x,id);
                /*switch(d.x) {
                  case 0:
                     return '#4caf50';
                  case 1:
                     return '#fdc141';
                  case 3:
                      return '#1f77b4';
                  default:
                      return '#f7665b';
                }*/
              }
            }
          },
          legend: {
              show: false
           },
          padding: {
             left: -9
          },
          bar: {
             width: 5
          },
          axis: {
            x: {
                  min: 0,
                  max: data.length,
                  tick:{
                        outer: false
                      },
                  show:true,
                  type: 'category' // this needed to load string x value
              }
          },tooltip: {
                format: {
                  title: function (d) { return getTooltip(d,id) }
           }          
          }
      });
      function getColor(x,id){
        var color='';
        switch(id) {
                  case "csat_detail":
                    if(x<2) color='#f7665b';
                    if(x==2) color='#fdc141';
                    if(x>2) color='#4caf50';
                    break;
                  case "nps_detail":
                    if(x<7) color='#f7665b';
                    if(x>6 && x<9) color='#fdc141';
                    if(x>8) color='#4caf50';
                    break;
                  case "ces_detail":
                    color='#1f77b4';
                    break;
           }
        return color;     
      }
         function getTooltip(x,id){
        var tooltip='';
        switch(id) {
                  case "csat_detail":
                    if(x<2) tooltip='Unsatisfied';
                    if(x==2) tooltip='Neutral';
                    if(x>2) tooltip='Satisfied';
                    break;
                  case "nps_detail":
                    if(x<7) tooltip='Detractors';
                    if(x>6 && x<9) tooltip='Neutrals';
                    if(x>8) tooltip='Promoters';
                    break;
                  case "ces_detail":
                    tooltip='';
                    break;
           }
        return tooltip;     
      }
    }
}
   