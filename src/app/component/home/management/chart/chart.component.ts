import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import * as D3 from 'd3';
import * as C3 from 'c3';
declare var $:any;
import { ManagementService } from 'app/service/management/index';
import { Chart } from 'app/model/chart.model';
import { AppConfig } from "app/config/app.config";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
})
export class ChartComponent implements OnInit {
   row1Extra: any;
   row2Extra: any;
   col:number;
   numberClass:any;
   scoreWeek:any;
   listcharts : Chart[];
   listScoreWeekChart:Chart[];
   listExtraChart:Chart[];
   isOnInit:boolean;
   constructor(
       private managementService: ManagementService,
       private router: Router
   ){
   }

  ngOnInit() {

     var self=this;
     self.row1Extra=[];
     self.row2Extra=[];
     this.isOnInit=false;

     this.managementService.getCharts().then(p => {
        self.listcharts=p;
        self.listScoreWeekChart=self.listcharts.filter((chart) => {return chart.type=="scoreweek"});
        self.listExtraChart=self.listcharts.filter((chart) => {return chart.type=="extra"});
        setHTMLDefault(self.listExtraChart); 
        this.isOnInit=true;
        self.afterViewInit();
     });
        
        //add html when open
        function setHTMLDefault(listExtraFeedback:any){
            let length :any=(listExtraFeedback.length+1)/2;
            self.col= parseInt(length);
            self.numberClass=12/self.col;
            let row1Length;
            let row2Length;
            switch(self.col) {
                  case 2:
                    row1Length=2;
                    row2Length=2;
                    break;
                  case 3:
                    row1Length=3;
                    row2Length=listExtraFeedback.length-3;
                    break;
                  case 4:
                    row1Length=4;
                    row2Length=listExtraFeedback.length-4;
                    break;
                }
            listExtraFeedback.forEach((chart,i)=>{
                  if(i<row1Length) 
                    self.row1Extra.push(chart)
                  else 
                    self.row2Extra.push(chart)
                })
          }
    }
    onSelect(chart: Chart){
          this.router.navigate(['/chartDetail',chart.id]);
    }
    afterViewInit() {

        var self=this;
      // Create WebSocket connection.
        const socket = new WebSocket(AppConfig.ApiRoot.replace("http", "ws")+'/websocket/chart/'+JSON.parse(localStorage.getItem('currentUser')).username);
      // Listen for messages
        socket.addEventListener('message', function (event) {
           if(!self.isOnInit) return;
           self.listcharts=JSON.parse(event.data);
           self.row1Extra.forEach((chart,i)=> {
                //self.row1Extra[i]=self.listcharts.filter((c) =>{return chart.id==c.id})[0];
                self.row1Extra[i].small_quote=self.listcharts.filter((c) =>{return chart.id==c.id})[0].small_quote;
                self.row1Extra[i].blockquote=self.listcharts.filter((c) =>{return chart.id==c.id})[0].blockquote;
            })
            self.row2Extra.forEach((chart,i)=> {
                //chart=self.listcharts.filter((c) =>{return chart.id==c.id})[0];
                self.row2Extra[i].small_quote=self.listcharts.filter((c) =>{return chart.id==c.id})[0].small_quote;
                self.row2Extra[i].blockquote=self.listcharts.filter((c) =>{return chart.id==c.id})[0].blockquote;
            })
           if(!self.scoreWeek)createCharts(JSON.parse(event.data));
           else loadCharts(JSON.parse(event.data));
        });
        var DonutChart=[];
        var BarChart=[];
        //create charts when open
        function createCharts(data){
            self.scoreWeek={}
            let i=1;
            data.forEach((chart,j)=> {
                if(chart.type=="extra"){
                     DonutChart[j]=self.createDonutChart("#idDonutChart"+chart.id,chart.data.circle,self.getColorFromLevel(chart.level));
                     BarChart[j]=self.createBarChartRotated("#idBarChart"+chart.id,chart.data.bar);
                }else 
                {
                      self.scoreWeek[chart.id]=self.createScoreWeek("#WeekScore"+chart.id,chart.data);
                }
                  i++
            });       
        }
        //reload charts.
        function loadCharts(data){
            let i=1;
            DonutChart=[];
            BarChart=[];
            data.forEach((chart,j)=> {
                if(chart.type=="extra"){
                     DonutChart[j]=self.createDonutChart("#idDonutChart"+chart.id,chart.data.circle,self.getColorFromLevel(chart.level));
                     BarChart[j]=self.createBarChartRotated("#idBarChart"+chart.id,chart.data.bar);
                }else self.scoreWeek[chart.id].load({
                        columns:chart.data
                  })
                  i++
            });  
            // $(".c3-axis-y .domain").css("display","none");    
            // $(".c3-axis-y .tick>line").css("display","none");
        }
    }
    getColorFromLevel(level){
        switch(level) {
                  case 1:
                    return '#4caf50'
                  case 0:
                   return '#fdc141'
                  case -1:
                    return '#f7665b'
                }
    }
    getExtraCharts(listExtraFeedback){
        var self=this;
        self.row1Extra=[];
        self.row2Extra=[];
        let length :any=(listExtraFeedback.length+1)/2;
            self.col= parseInt(length);
            self.numberClass=12/self.col;
            let row1Length;
            let row2Length;
            switch(self.col) {
                  case 2:
                    row1Length=2;
                    row2Length=2;
                    break;
                  case 3:
                    row1Length=3;
                    row2Length=listExtraFeedback.length-3;
                    break;
                  case 4:
                    row1Length=4;
                    row2Length=listExtraFeedback.length-4;
                    break;
                }
            listExtraFeedback.forEach((chart,i)=>{
               if(i<row1Length) 
                    self.row1Extra.push(chart)
                  else 
                    self.row2Extra.push(chart)      
                })
    }
    createScoreWeek(idDiv,data:any) {
        return C3.generate({
                bindto: idDiv,
                padding: {
                      top: 10,
                      right: 10,
                      bottom: 40,
                      left: 25
                },
                size: {
                      height: 300
                },
                data: {
                    x:'x',
                    columns: data,
                    type: 'area'
                      ,
                    labels: true
                },
                legend: {
                    show: false
                },
                color: {
                      pattern: ['#8edcf7']
                },
                axis: {
                      y: {
                        min: -80,
                        max: 80,
                        tick:{
                            outer: false
                        }
                    },
                     x: {
                     type: 'categories'
                    }
                }
            });    
    }
    createBarChartRotated(idDiv :string,data:any) {
          var max = data.reduce(function(a, b) {
              return Math.max(a, b);
          });
          return C3.generate({
          bindto: idDiv,
          data: {
              columns: [data],
            type: 'bar',
            labels: true,
            colors: {
              data1: function(d) {
                switch(d.x) {
                  case 0:
                     return '#f7665b';
                  case 1:
                     return '#fdc141';
                  case 2:
                      return '#4caf50';
                  default:
                      return '#1f77b4';
                }
              }
            }
          },
          legend: {
              show: false
           },
          padding: {
             left: -18
          },
          bar: {
             width: 5
          },
          axis: {
            rotated: true,
            y: {
                  min: 0,
                  max: max,
                  tick:{
                        outer: false
                      },
                  show:false
              }
          },tooltip: {
            show:false
          }
      });
    }
    createDonutChart(idDiv :string, percentage: number,color:any) {
        $(idDiv).empty();
        return C3.generate({
        bindto: idDiv,
        padding: {
                      top: 10,
                      right:30,
                      left: 0,
                      bottom:0
                },
        data: {
           columns: [
               ['data1', percentage],
               ['data2', 100-percentage]
            ],
           type : 'donut',
             order: null,
           colors: {
            data1: color,
            data2: '#f3f0f0',
        },
        },
        legend: {
              show: false
        },
        donut: {
          title: percentage+"%",
          width: 5,
          expand: false,
          label: {
              show: false
          }
        }
    });        
    }
}
