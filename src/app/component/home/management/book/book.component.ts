import { Component, OnInit,ViewContainerRef,NgZone } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { ManagementService } from 'app/service/management/index';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  books: any;
  page: number = 1;
  totalPage: number;
  message:String;
  refusedBook:any;

  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private zone: NgZone        
  ) {
    this.toastr.setRootViewContainerRef(vcr);      
  }

  ngOnInit() {
    this.getBooks();
  }

  nextPage(): void {
    if (this.page < this.totalPage) this.page++;
    this.getBooks();    
  }

  previousPage(): void {
    if (this.page > 1) this.page--;
    this.getBooks();    
  }

  getBooks(): void {
    this.managementService.getBooks(this.page).then(p => {
      this.books = p['data'];;//this.getCustomers(p);
      this.totalPage = p['totalPage'];        
    });
  }

  acceptBook(bookId){
    var book=this.books.filter(book=>
        {return book._id==bookId}
    )
    if (book.length>0) {
      book=book[0];
      book.isAccept=true;
      book.isDecided=true;
      this.managementService.updateBook(book)
      .then((success: boolean) => {
        if (success){
           alert("Your response is sending !!!");
           this.loadBookPage()
          //  this.reloadPage(); 
        }
      });
    }
  }
  refuseBook(bookId){
    var book=this.books.filter(book=>
        {return book._id==bookId}
    )
    if (book.length>0) {
      this.refusedBook=book[0];
    }
  }
  addRefuseReason(){
    this.refusedBook.isAccept=false;
    this.refusedBook.isDecided=true;
    this.refusedBook.reasonRefuse=this.message;      
    this.managementService.updateBook(this.refusedBook)
    .then((success: boolean) => {
      if (success){
         alert("Your response is sending !!!");
         this.loadBookPage()
      }
      else {
        this.toastr.error('Your input is not correct, please check your business...', 'Oops! ');
      }
    });
  }
  loadBookPage(){
    this.router.navigate(["/book"]);    
  }
}