import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { ManagementService } from 'app/service/management/index';
import { Asset } from 'app/model/asset.model';
import { MessageConstants } from 'app/constant/message.constant';

@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css']
})

export class AssetComponent implements OnInit {
  code: String;
  asset: Asset = new Asset();
  loading: boolean = false;

  loadingInform: string = MessageConstants.Inprogress;

  constructor(
    private managementService: ManagementService,
    private router: Router,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  buyCoin(): void {
    this.loading = true;
    this.managementService.buyCoin(this.code)
      .then((res) => {
        this.loading = false;
        if (res == null){
          this.toastr.warning('Your code is not existing.', 'Alert!');
        }else{
          this.toastr.success('Get '+ res.coin +' coins successfully!', 'Success!');
          this.code = '';
        }
      });
  }

}