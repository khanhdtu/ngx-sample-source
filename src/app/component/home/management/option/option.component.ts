import { Component, OnInit, ViewChild} from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import {ModalModule} from "ng2-modal";

import { ManagementService } from 'app/service/management/index';
import { Options } from 'app/model/options.model';

@Component({
  selector: 'app-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.css']
})

export class OptionComponent implements OnInit {
  options : Options[];

  constructor(
    private managementService: ManagementService,
  ) {
  }

  ngOnInit() {
    this.managementService.getOptions().then(p => {
       this.options = p;
      });
  }
}