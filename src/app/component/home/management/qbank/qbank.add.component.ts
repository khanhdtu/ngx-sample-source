import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormsModule, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ModalModule } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ButtonConstants } from 'app/constant/button.constant';

import { AppConfig } from "app/config/app.config";
import { ManagementService } from 'app/service/management/index';
import { Qbank } from 'app/model/qbank.model';
import { Answer } from 'app/model/answer.model';

@Component({
  selector: 'app-add-qbank',
  templateUrl: './qbank.add.component.html',
  styleUrls: ['./qbank.component.css']
})

export class QbankAddComponent implements OnInit {
  item: Qbank = new Qbank();
  answer1: Answer = new Answer();
  answer2: Answer = new Answer();
  submitButtonText: string = ButtonConstants.Create;
  loading: boolean = false;
  isValid: boolean = true;
  files: File[] = [];
  formData: FormData = new FormData();
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;
  answerType: Number = 1;
  createForm: FormGroup;

  constructor(
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private managementService: ManagementService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.toastr.setRootViewContainerRef(vcr);
    this.createForm = fb.group({
      'content': ['name_default', Validators.required]
    });
  }

  ngOnInit() {
    let itemId = this.activedRoute.snapshot.params.id;
    if (itemId != undefined && itemId != 0) {
      this.submitButtonText = ButtonConstants.Update;
      this.managementService.getQbankById(itemId)
        .then(res => {
          this.item = res;
          this.answer1 = res.answers[0];
          this.answer2 = res.answers[1];
          this.answerType = res.answers[0].isImage ? 2 : 1;
        });
    }
  }

  addOrUpdate(): void {
    if (!this.isValid) return;
    this.loading = true;
    if (this.answerType == 2) {
      this.answer1.isImage = true;
      this.answer2.isImage = true;
    } else {
      this.formData = null;
    }
    this.item.answers = [];
    this.item.answers.push(this.answer1);
    this.item.answers.push(this.answer2);
    //
    this.managementService.addQbank(this.item, this.formData)
      .then((success: boolean) => {
        this.loading = false;
        if (success)
          this.router.navigate(["/qbank"]);
      });
  }

  fileChange(event): void {
    let _URL = window.URL;
    let fileList: FileList = event.target.files;
    for (var i = 0; i < fileList.length; i++) {
      let file: File = fileList[i];
      if (file != null) {
        this.formData.append('uploadFile' + i, file, file.name);
      }
    }
    // if (fileList.length > 0) {
    //   this.file = fileList[0];

    //   // Validate upload file
    //   let img = new Image();

    //   img.onload = () => {
    //     let width = img.naturalWidth;
    //     let height = img.naturalHeight;
    //     if (width != height) {
    //       this.isValid = false;
    //       this.toastr.error('Your file is not correct, please check your business...', 'Oops! ');
    //     } else {
    //       this.toastr.info('Right...', 'Oops! ');
    //     }
    //   };
    //   img.src = _URL.createObjectURL(this.file);
    // }
  }
}