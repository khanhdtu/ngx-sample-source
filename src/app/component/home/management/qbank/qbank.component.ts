import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { ModalModule } from "ng2-modal";
import { AppConfig } from "app/config/app.config";

import { ManagementService } from 'app/service/management/index';
import { Qbank } from 'app/model/qbank.model';

@Component({
  selector: 'app-qbank',
  templateUrl: './qbank.component.html',
  styleUrls: ['./qbank.component.css']
})

export class QbankComponent implements OnInit {
  qbank: Qbank[];
  apiUrl: string = AppConfig.ApiRoot;
  staticUrl: string = AppConfig.StaticRoot;

  constructor(
    private managementService: ManagementService,
  ) { 
  }

  ngOnInit() {
    this.managementService.getQbank()
      .then(res => {
        this.qbank = res;
      });
  }
}