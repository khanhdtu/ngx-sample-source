import { Component, OnInit } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { ManagementService } from 'app/service/management/index';
import { Criteria } from 'app/model/criteria.model';
import { Customer } from 'app/model/customer.model';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})

export class CustomerComponent implements OnInit {
  page: number = 1;
  totalPage: number;
  criteria: Criteria = new Criteria();
  customer: Customer = null;
  customers: Customer[];

  constructor(
    private managementService: ManagementService
  ) {
  }

  ngOnInit() {
    this.getCustomers(this.criteria);
  }

  nextPage(): void {
    if (this.page < this.totalPage) this.page++;
    this.criteria = new Criteria(this.page);
    this.getCustomers(this.criteria);
  }

  previousPage(): void {
    if (this.page > 1) this.page--;
    this.criteria = new Criteria(this.page);
    this.getCustomers(this.criteria);
  }

  getCustomers(criteria): void {
    this.managementService.getCustomers(criteria).then(p => {
      this.customers = p['data'];
      this.totalPage = p['totalPage'];
    });
  }
}