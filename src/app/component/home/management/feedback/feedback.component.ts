import { Component, OnInit } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { ManagementService } from 'app/service/management/index';
import { Criteria } from 'app/model/criteria.model';
import { Criterion } from 'app/model/criterion.model';
import { Options } from 'app/model/options.model';
import { Feedback } from 'app/model/feedback.model';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})

export class FeedbackComponent implements OnInit {
  result: Object;
  feedbacks: Feedback[];
  options: Options[];
  criterions: Criterion[];
  page: number = 1;
  totalPage: number = 1;
  criteria: Criteria = new Criteria();
  detail: Feedback = null;

  constructor(
    private managementService: ManagementService
  ) {
  }

  ngOnInit() {
    this.getFeedBack(this.criteria);
    this.managementService.getCriterions().then(p => {
      this.criterions = p;
    });
    this.managementService.getOptions().then(p => {
      this.options = p;
    });
  }

  nextPage(): void {
    if (this.page < this.totalPage) this.page++;
    this.criteria = new Criteria(this.page);
    this.getFeedBack(this.criteria);
  }

  previousPage(): void {
    if (this.page > 1) this.page--;
    this.criteria = new Criteria(this.page);
    this.getFeedBack(this.criteria);
  }

  getFeedBack(criteria): void {
    this.managementService.getFeedbacks(criteria).then(p => {
      this.feedbacks = p['data'];
      this.totalPage = p['totalPage'];
    });
  }

  view(fb): void {
    this.detail = fb;
  }
}