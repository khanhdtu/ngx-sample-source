import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';

import { ManagementService } from 'app/service/management/index';
import { Discount } from 'app/model/discount.model';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})

export class DiscountComponent implements OnInit {
  ces: Discount[];
  rank: Discount[];

  constructor(
    private managementService: ManagementService,
  ) {
  }

  ngOnInit() {
    this.managementService.getDiscounts().then(p => {
      this.ces = p["ces"];
      this.rank = p["rank"];
    });
  }
}