import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { MaterializeAction, toast  } from 'angular2-materialize';

import { ManagementService } from 'app/service/management/index';
import { Discount } from 'app/model/discount.model';

@Component({
  selector: 'app-add-discount',
  templateUrl: './discount.add.component.html',
  styleUrls: ['./discount.component.css']
})

export class DiscountAddComponent implements OnInit {
  item: Discount = new Discount();
  loading: boolean = false;

  constructor(
    private managementService: ManagementService,
    private activedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.loading = true;
    let id = this.activedRoute.snapshot.params.id;
    if (id != undefined) {
      this.managementService.getDiscountById(id)
        .then(res => {
          this.loading = false;
          this.item = res;
        });
    }
  }

  addOrUpdate(): void{
    this.loading = true;
    this.managementService.addOrUpdateDiscount(this.item)
      .then(res =>{
        this.loading = false;
        let success = res["success"];
        let type = success ? 'toast-success' : 'toast-error';
        toast(res["message"], 3000, type);
      });
  }
}