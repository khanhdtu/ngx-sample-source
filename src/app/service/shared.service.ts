import { Injectable } from '@angular/core';
import { HttpService } from 'app/helpers/httpService';
import { AppConfig } from "app/config/app.config";
import { Localization } from "app/model/localization.model";

@Injectable()
export class SharedService {
    lang: String = 'en';
    restriction: String;
    localizations: Localization[] = [];
    localLoaded: boolean = false;

    constructor(private httpService: HttpService) {
        this.trackProvider();
    }

    trackProvider(): void{
        var str = localStorage.getItem('currentUser');
        if (str != null && str != '') {
            var provider = JSON.parse(str);
            this.restriction = provider.restriction;
            this.lang = provider.lang != undefined ? provider.lang : 'vi';
        }
    }

    get(field: String): String {
        if (this.localizations,length ==0 ){
            var str = localStorage.getItem('localizations');
            if (str != null && str != '') {
                this.localizations = JSON.parse(str);
            }else{
                this.getLocalizations();
            }
        }
        var lst = this.localizations.filter(item => item.field == field);
        if (lst.length > 0) {
            var obj = lst[0];
            var rs = obj.values.filter(i => i.language == this.lang)[0];
            if (rs != undefined)
                return rs.content;
        }

        return field;
    }

    getLocalizations(): Promise<void> {
        this.trackProvider();
        var url = AppConfig.ApiRoot + '/provider/localization';
        return this.httpService.getwithcallback(url, res => {
            this.localizations = res.json();
            localStorage.setItem('localizations', JSON.stringify(this.localizations));
            this.localLoaded = true;
        });
    }




}