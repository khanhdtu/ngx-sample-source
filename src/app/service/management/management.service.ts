import { Injectable } from '@angular/core';
import { Response }  from "@angular/http";
import 'rxjs/add/operator/toPromise';

import { AppConfig } from 'app/config/app.config';
import { ManagementConfig } from 'app/config/management/management.config';
import { AuthService } from 'app/service/auth/index';

import { HttpService } from 'app/helpers/httpService';

import { ProviderModel } from 'app/model/provider.model';
import { Criteria } from 'app/model/criteria.model';
import { Chart } from 'app/model/chart.model';
import { Asset } from 'app/model/asset.model';
import { Criterion } from 'app/model/criterion.model';
import { Menu } from 'app/model/menu.model';
import { Qbank } from 'app/model/qbank.model';
import { Gift } from 'app/model/gift.model';
import { CustomerGift } from 'app/model/customerGift.model';
import { Report } from 'app/model/report.model';
import { Options } from 'app/model/options.model';
import { Customer } from 'app/model/customer.model';
import { Discount } from 'app/model/discount.model';
import { Res } from 'app/model/res.model';

@Injectable()
export class ManagementService {
  username: string;

  constructor(
    private httpService: HttpService,
    private authService: AuthService
  ) {

  }

  getProfile(): Promise<ProviderModel> {
    let url = ManagementConfig.GetProviderByUsernameUrl;

    return this.httpService.getwithcallback(url, res => res.json());
  }

  changePassword(op, np): Promise<Res> {
    let url = ManagementConfig.ChangePasswordUrl + '?op=' + op + '&np=' + np;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getQRCode(billValue: number): Promise<string> {
    let url = ManagementConfig.GetQRCodeUrl + '?value=' + billValue;

    return this.httpService.getwithcallback(url, res => res.json() as string);
  }

  getMessengerCode(billNo: number): Promise<string> {
    let url = ManagementConfig.GetMessengerCodeUrl + '?billNo=' + billNo;

    return this.httpService.getwithcallback(url, res => res.json());
  }

  buyCoin(code: String): Promise<Asset> {
    let url = ManagementConfig.BuyCoinUrl + '?code=' + code;

    return this.httpService.getwithcallback(url, res => res.json());
  }


  getHistories(): Promise<Object[]> {
    let criteria = JSON.stringify(new Criteria());
    let url = ManagementConfig.GetHistoryUrl;

    return this.httpService.getwithcallback(url, res => res.json());
  }

  getFeedbacks(criteria: Criteria): Promise<Object> {
    let url = ManagementConfig.GetFeedbackUrl;

    return this.httpService.postwithcallback(url, criteria, res => res.json());
  }

  getQuestions(): Promise<Object[]> {
    let url = ManagementConfig.GetQuestionUrl;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getOptions(): Promise<Options[]> {
    let url = ManagementConfig.GetOptionUrl;

    return this.httpService.getwithcallback(url, res => res.json());
  }

  getCharts(): Promise<Chart[]> {
    let url = ManagementConfig.GetChartsUrl;

    return this.httpService.getwithcallback(url, res => res.json());
  }

  getCriterions(): Promise<Criterion[]> {
    let url = ManagementConfig.GetCriterionsUrl;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getCriterionById(criterionId: Number): Promise<Criterion> {
    let url = ManagementConfig.GetCriterionByIdUrl + '?id=' + criterionId;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getCriterionByName(name: String): Promise<Criterion> {
    let url = ManagementConfig.GetCriterionByIdUrl + '?name=' + name;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  addCriterion(criterion: Criterion, formData: FormData): Promise<boolean> {
    let newImage: string;
    if (formData != null) {
      let iconUrl = ManagementConfig.UploadImageUrl + '?type=criteria';
      return this.httpService.upload(iconUrl, formData)
        .toPromise()
        .then(res => {
          newImage = res.json();
        }
        , err => {
          console.log(err);
        })
        .then(() => {
          criterion.isUpload = true;
          criterion.iconName = newImage;
          return this.addCriteria(criterion);
        })
        .catch(this.handleError);
    } else {
      criterion.isUpload = false;
      return this.addCriteria(criterion);
    }
  }

  addCriteria(criterion: Criterion): Promise<boolean> {
    let url = ManagementConfig.AddCriterionUrl;
    return this.httpService.postwithcallback(url, JSON.stringify(criterion), res => res.json(),);
  }

  deleteCriterion(criterionId: string): Promise<boolean> {
    let url = ManagementConfig.DeleteCriterionUrl + '?criterionId=' + criterionId;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getMenu(): Promise<Menu[]> {
    let url = ManagementConfig.GetMenuUrl;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getItemOnMenuById(id: Number): Promise<Menu> {
    let url = ManagementConfig.GetItemOnMenuUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  addItemToMenu(item: Menu, formData?: FormData): Promise<boolean> {
    let newImage: string;
    if (formData != null) {
      let iconUrl = ManagementConfig.UploadImageUrl + '?type=menu';
      return this.httpService.upload(iconUrl, formData)
        .toPromise()
        .then(res => {
          newImage = res.json();
        }
        , err => {
          console.log(err);
        })
        .then(() => {
          item.thumbnail = newImage;
          return this.addItemToMenu(item);
        })
        .catch(this.handleError);
    } else {
      let url = ManagementConfig.AddItemToMenuUrl;
      return this.httpService.postwithcallback(url, JSON.stringify(item), res => res.json());
    }
  }

  deleteItemOnMenu(id: string): Promise<boolean> {
    let url = ManagementConfig.DeleteItemOnMenuUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getQbank(): Promise<Qbank[]> {
    let url = ManagementConfig.GetQbankUrl;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getQbankById(id: String): Promise<Qbank> {
    let url = ManagementConfig.GetQbankByIdUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  addQbank(item: Qbank, formData?: FormData): Promise<boolean> {
    let newImage: String[];
    if (formData != null) {
      let url = ManagementConfig.UploadImageUrl + '?type=qbank';
      return this.httpService.upload(url, formData)
        .toPromise()
        .then(res => {
          newImage = res.json();
        }
        , err => {
          console.log(err);
        })
        .then(() => {
          for (var i = 0; i < item.answers.length; i++) {
            item.answers[i].content = newImage[i];
          }
          return this.addQbank(item);
        })
        .catch(this.handleError);
    } else {
      let url = ManagementConfig.AddQbankUrl;
      return this.httpService.postwithcallback(url, JSON.stringify(item), res => res.json());
    }
  }

  deleteQbank(id: string): Promise<boolean> {
    let url = ManagementConfig.DeleteQbankUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }


  getGifts(criteria: Criteria): Promise<Gift[]> {
    let url = ManagementConfig.GetGiftUrl;
    return this.httpService.postwithcallback(url, criteria, res => res.json());
  }

  getGiftByCode(code: string): Promise<CustomerGift> {
    let url = ManagementConfig.GetGiftByCodeUrl + '?code=' + code;
    return this.httpService.getwithcallback(url, res =>  res.json());
  }

  useGiftCode(code: string): Promise<boolean> {
    let url = ManagementConfig.UseGiftCodeUrl + '?code=' + code;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getGiftHistory(criteria: Criteria): Promise<CustomerGift[]>{
    let url = ManagementConfig.GetGiftHistoryUrl;
    return this.httpService.postwithcallback(url, criteria, res => res.json());
  }

  getStaffs(): Promise<Object[]> {
    let url = ManagementConfig.GetStaffsUrl;
    return this.httpService.get(url)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  deleteStaff(StaffId): Promise<boolean> {
    let url = ManagementConfig.DeleteStaffUrl + '?staffId=' + StaffId;
    return this.httpService.get(url)
      .toPromise()
      .then(res => {
        console.log(res);
        return res.json();
      }
      , err => {
        console.log(err);
      })
      .catch(this.handleError);
  }

  getGiftById(id: String): Promise<Gift> {
    let url = ManagementConfig.GetGiftByIdUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  addGift(item: Gift, formData?: FormData): Promise<boolean> {
    let newImage: String[];
    if (formData != null) {
      let url = ManagementConfig.UploadImageUrl + '?type=gift';
      return this.httpService.upload(url, formData)
        .toPromise()
        .then(res => {
          newImage = res.json();
        }
        , err => {
          console.log(err);
        })
        .then(() => {
          item.thumbnail = newImage[0].toString();
          return this.addGift(item);
        })
        .catch(this.handleError);
    } else {
      let url = ManagementConfig.AddGiftUrl;
      let str = item.expireDate.toString();
      item.expireDate = new Date(str.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
      return this.httpService.postwithcallback(url, JSON.stringify(item), res => res.json());
    }
  }

  deleteGift(id: string): Promise<boolean> {
    let url = ManagementConfig.DeleteGiftByIdUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }


  getReports(): Promise<Report[]> {
    let url = ManagementConfig.GetReportsUrl;

    return this.httpService.getwithcallback(url, res => JSON.parse(res.json()));
  }

  getPromotion(): Promise<Object[]> {
    let url = ManagementConfig.GetPromotionUrl;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  addPromotion(promotion: any, formData: FormData): Promise<any> {
    let newImage: string;
    let iconUrl = ManagementConfig.UploadImageUrl + '?type=promotion';
    if (formData != null) {
      return this.httpService.upload(iconUrl, formData)
        .toPromise()
        .then(res => {
          newImage = res.json();
        }
        , err => {
          console.log(err);
        })
        .then(() => {
          promotion.linkImage = newImage;
          let url = ManagementConfig.AddPromotionUrl;
          return this.httpService.postwithcallback(url, JSON.stringify(promotion), res => res.json());
        })
        .catch(this.handleError);
    } else {
      let url = ManagementConfig.AddPromotionUrl;
      return this.httpService.postwithcallback(url, JSON.stringify(promotion), res => res.json());
    }
  }

  uploadBackground(formData: FormData): Promise<boolean> {
    let bgUrl = ManagementConfig.UploadImageUrl + '?type=bg';
    return this.httpService.upload(bgUrl, formData)
            .toPromise().then(res => res.json());
  }

  getMessengerCodeStaff(): Promise<string> {
    let url = ManagementConfig.GetMessengerCodeStaffUrl;

    return this.httpService.getwithcallback(url, res => res.json());
  }

  getPromotions(page: any): Promise<Object> {
    var itemsPerPage = 10;
    let url = ManagementConfig.GetAllPromotionsUrl;

    return this.httpService.postwithcallback(url, { itemsPerPage: itemsPerPage, page: page }, res => res.json());
  }

  getPromotionById(promotionId: any): Promise<Object[]> {
    let url = ManagementConfig.GetPromotionByIdUrl + '?id=' + promotionId;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getCustomers(criteria: Criteria): Promise<Customer[]> {
    let url = ManagementConfig.GetCustomerUrl;
    return this.httpService.postwithcallback(url, criteria, res => {
      return res.json();
    });
  }

  getCustomerById(id: string): Promise<Customer> {
    let url = ManagementConfig.GetCustomerByIdUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getBooks(page: any): Promise<Object> {
    var itemsPerPage = 10;
    let url = ManagementConfig.GetBooksUrl;

    return this.httpService.postwithcallback(url, { itemsPerPage: itemsPerPage, page: page }, res => res.json());
  }

  updateBook(book: any): Promise<any> {
    let url = ManagementConfig.updateBookUrl;
    return this.httpService.postwithcallback(url, JSON.stringify(book), res => res.json());
  }

  responsesBadcomment(mess: any): Promise<any> {
    let url = ManagementConfig.ResponsesBadcommentUrl;
    return this.httpService.postwithcallback(url, JSON.stringify(mess), res => res.json());
  }

  getDiscounts(): Promise<Discount[]> {
    let url = ManagementConfig.GetDiscountUrl;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  getDiscountById(id: string): Promise<Discount> {
    let url = ManagementConfig.GetDiscountByIdUrl + '?id=' + id;
    return this.httpService.getwithcallback(url, res => res.json());
  }

  addOrUpdateDiscount(discount: Discount): Promise<Res> {
    let url = ManagementConfig.AddOrUpdateDiscountUrl;
    return this.httpService.postwithcallback(url, JSON.stringify(discount), res => res.json());
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}