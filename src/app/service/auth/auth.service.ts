import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { ProviderModel } from 'app/model/provider.model';
import { Res } from 'app/model/res.model';

import { AppConfig } from 'app/config/app.config';
import { AuthConfig } from 'app/config/auth/auth.config';

import { HttpService } from 'app/helpers/httpService';
import { SharedService } from 'app/service/shared.service';

@Injectable()
export class AuthService {

  constructor(private httpService: HttpService, private sharedService: SharedService) { }
  
  login(username:string, password:string): Promise<boolean> {
    let data = JSON.stringify({username: username, password: password});
    let url = AuthConfig.LoginUrl;
    return this.httpService.post(url, data)
      .toPromise()
      .then((res) => {
        let provider = res.json();
        if (!provider){
          return false;
        }
        this.sharedService.restriction = provider.restriction;
        localStorage.setItem('currentUser', JSON.stringify(provider));
        return true;
      }, (err) => {
        console.log(err);
      }).catch(this.handleError);
  }

 resetPassword(username): Promise<Res> {
    let url = AuthConfig.ResetPasswordUrl + '?username=' + username + '&forgot=true';
    return this.httpService.getwithcallback(url, res => res.json());
  }
  
  register(provider: ProviderModel): Promise<boolean> {
    let url = AuthConfig.RegisterUrl;
    return this.httpService.postwithcallback(url, provider, res => res.json());
  }

  logout(): void{
    localStorage.removeItem('currentUser');
    localStorage.removeItem('localizations');
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
