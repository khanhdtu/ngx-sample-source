import { Socuty.Roulette.AppPage } from './app.po';

describe('socuty.roulette.app App', () => {
  let page: Socuty.Roulette.AppPage;

  beforeEach(() => {
    page = new Socuty.Roulette.AppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
